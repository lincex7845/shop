package com.mera.shop.controladores;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import com.mera.shop.entidades.Cliente;
import com.mera.shop.entidades.Inventario;
import com.mera.shop.entidades.PagoTarjetaCredito;
import com.mera.shop.entidades.Producto;
import com.mera.shop.entidades.Residencia;
import com.mera.shop.entidades.Transaccion;
import com.mera.shop.restricciones.TarjetaCreditoConstraint;
import com.mera.shop.restricciones.TextoConstraint;
import com.mera.shop.servicios.InventarioServicio;
import com.mera.shop.servicios.PagosServicio;
import com.mera.shop.servicios.TransaccionServicio;
import com.mera.shop.utilidades.DigestAlgorithm;
import com.mera.shop.utilidades.FranquiciaTarjetaCredito;
import com.mera.shop.utilidades.SeguridadUtil;
import com.payu.sdk.model.OrderStatus;
import com.payu.sdk.model.PaymentCountry;

@Controller
@Qualifier("checkoutController")
public class CheckoutController extends GenericForwardComposer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7042597550414067153L;

	private Producto productoSeleccionado;

	private InventarioServicio inventarioServicio;
	
	private TransaccionServicio transaccionServicio;

	private Textbox idClienteTextbox;
	private Textbox nombreTextbox;
	private Textbox emailTextbox;
	private Textbox direccionTextbox;
	private Textbox codigoPostalTextbox;
	private Textbox telefonoTextbox;
	private Textbox ciudadTextbox;
	private Textbox departamentoTextbox;

	private Radiogroup tarjetaRadioGroup;
	private Textbox numeroTarjetaTextbox;
	private Combobox anioCombobox;
	private Combobox mesCombobox;
	private Combobox coutasCombobox;
	private Textbox codigoTextbox;

	private Image imagenProductoImage;
	private Label descripcionProductoLabel;
	private Label valorProductoLabel;
	
	

	public Producto getProductoSeleccionado() {
		return this.productoSeleccionado;
	}

	@Autowired
	public void setInventarioServicio(InventarioServicio inventarioServicio) {
		this.inventarioServicio = inventarioServicio;
	}
	
	@Autowired
	public void setTransaccionesServicio(TransaccionServicio transaccionServicio) {
		this.transaccionServicio = transaccionServicio;
	}

	@Override
	public void doBeforeComposeChildren(Component comp) throws Exception {
		super.doBeforeComposeChildren(comp);
		HttpSession sesion = (HttpSession) Sessions.getCurrent().getNativeSession();
		if (sesion.getAttribute("producto") == null) {
			Messagebox.show("No se ha seleccionado ningun producto a comprar.", "Comprar Producto", Messagebox.OK,
					Messagebox.ERROR);
			Executions.sendRedirect("/index.zul");
		} else {
			productoSeleccionado = ((Inventario) sesion.getAttribute("producto")).getProducto();
		}
	}

	@Override
	public void doAfterCompose(Component component) throws Exception {
		super.doAfterCompose(component);
		coutasCombobox.setSelectedIndex(0);
		mesCombobox.setSelectedIndex(12);
		// a�os
		agregarAnios();
		// producto
		establecerProducto();
		// restricciones
		establecerRestricciones();
	}

	public void onCheck$tarjetaRadioGroup(ForwardEvent e) {
		FranquiciaTarjetaCredito franquicia = FranquiciaTarjetaCredito
				.valueOf(((Radio) e.getOrigin().getTarget()).getValue());
		numeroTarjetaTextbox.setConstraint(
				new TarjetaCreditoConstraint(franquicia));
		switch (franquicia) {
		case VISA:			
			codigoTextbox.setConstraint(
					new TextoConstraint(FranquiciaTarjetaCredito.VISA.getDigitosCodigoSeguridad(), "\\d+"));
			break;
		case AMEX:
			codigoTextbox.setConstraint(
					new TextoConstraint(FranquiciaTarjetaCredito.AMEX.getDigitosCodigoSeguridad(), "\\d+"));
			break;
		case MASTERCARD:
			codigoTextbox.setConstraint(
					new TextoConstraint(FranquiciaTarjetaCredito.MASTERCARD.getDigitosCodigoSeguridad(), "\\d+"));
			break;
		case DINNERS:
			codigoTextbox.setConstraint(
					new TextoConstraint(FranquiciaTarjetaCredito.DINNERS.getDigitosCodigoSeguridad(), "\\d+"));
			break;
		default:
			break;
		}
	}

	public void onClick$pagarButton(Event e) throws InterruptedException, NoSuchAlgorithmException, UnsupportedEncodingException {
		if (tarjetaRadioGroup.getSelectedItem() != null) {
			// Registrar cliente
			Cliente cliente = registrarCliente();
			// Registrar metodo pago
			PagoTarjetaCredito tarjeta = registrarTarjeta();
			// Crear transaccion
			HttpSession sesion = (HttpSession) Sessions.getCurrent().getNativeSession();
			String deviceSessionId = SeguridadUtil.computHexHashSignatureFromText(DigestAlgorithm.MD5,
					sesion.getId() + sesion.getCreationTime()) + cliente.getIdCliente();
			Transaccion transaccion = new Transaccion(SeguridadUtil.secureUID(), productoSeleccionado.getIdProducto(),
					cliente.getIdCliente(), deviceSessionId, SeguridadUtil.obtenerIPAddresZKRequest(), sesion.getId(),
					SeguridadUtil.obtenerUserAgentBrowserZKRequest());
			transaccionServicio.agregarNuevoElemento(transaccion);
			// Realizar Pago
			PagosServicio.realizarPago(transaccion, productoSeleccionado, cliente, tarjeta);
			// Consultar estado
			OrderStatus estadoOrden = PagosServicio.consultarEstadoPago(transaccion);
			// Descontar item - Informar al usuario			
			if(estadoOrden.equals(OrderStatus.CAPTURED)){
				Inventario registroInventario = inventarioServicio.obtenerElementoPorId(productoSeleccionado.getIdProducto());
				registroInventario.setCantidadDisponible(registroInventario.getCantidadDisponible()-1);
				inventarioServicio.actualizarElemento(registroInventario);
				Messagebox.show("�Gracias por su compra!. Le invitamos a seguir comprando.", "Comprar Producto", Messagebox.OK,
						Messagebox.INFORMATION, new org.zkoss.zk.ui.event.EventListener() {
							@Override
							public void onEvent(Event evt) throws InterruptedException {
								if (Messagebox.ON_OK.equals(evt.getName())) {
									// Volver Index
									volverIndex();
								}
							}
						});				
			}
			else if(estadoOrden.equals(OrderStatus.IN_PROGRESS)){
				Messagebox.show("Su compra esta en curso, para mayor informaci�n consulte con el administrador del sistema. Le invitamos a seguir comprando.", "Comprar Producto", Messagebox.OK,
						Messagebox.INFORMATION, new org.zkoss.zk.ui.event.EventListener() {
							@Override
							public void onEvent(Event evt) throws InterruptedException {
								if (Messagebox.ON_OK.equals(evt.getName())) {
									// Volver Index
									volverIndex();
								}
							}
						});	
			}
			else{
				Messagebox.show("Ocurri� un problema con su compra, para mayor informaci�n consulte con el administrador del sistema.", "Comprar Producto", Messagebox.OK,
						Messagebox.ERROR, new org.zkoss.zk.ui.event.EventListener() {
							@Override
							public void onEvent(Event evt) throws InterruptedException {
								if (Messagebox.ON_OK.equals(evt.getName())) {
									// Volver Index
									volverIndex();
								}
							}
						});
			}
			

		} else {
			Messagebox.show("Seleccione una franquicia para continuar con la compra", "Comprar Producto", Messagebox.OK,
					Messagebox.ERROR);
		}
	}

	public void onClick$cancelarButton(Event e) {
		volverIndex();
	}
	
	private void volverIndex(){
		if (productoSeleccionado != null) {
			session.removeAttribute("producto");
			Executions.sendRedirect("/index.zul");
		}
	}

	private PagoTarjetaCredito registrarTarjeta() {
		return new PagoTarjetaCredito(
				numeroTarjetaTextbox.getValue(), 
				Integer.valueOf(anioCombobox.getValue()),
				Integer.valueOf(mesCombobox.getValue()), Integer.valueOf(codigoTextbox.getValue()),
				FranquiciaTarjetaCredito.valueOf(tarjetaRadioGroup.getSelectedItem().getValue()),
				Integer.valueOf((String)coutasCombobox.getSelectedItem().getValue()));

	}

	private Cliente registrarCliente() {
		Residencia residencia = new Residencia(direccionTextbox.getValue(), codigoPostalTextbox.getValue(),
				ciudadTextbox.getValue(), departamentoTextbox.getValue(), PaymentCountry.CO.name());
		return new Cliente(idClienteTextbox.getValue(), nombreTextbox.getValue(), emailTextbox.getValue(),
				telefonoTextbox.getValue(), residencia);
	}

	private void agregarAnios() {
		int anioActual = Calendar.getInstance().get(Calendar.YEAR);
		for (int i = anioActual; i <= anioActual + 10; i++) {
			anioCombobox.appendItem(Integer.toString(i));
		}
		anioCombobox.setSelectedIndex(1);
	}

	private void establecerProducto() {
		imagenProductoImage.setSrc(productoSeleccionado.getRutaImagenProducto());
		descripcionProductoLabel.setValue(productoSeleccionado.getDescripcionProducto());
		valorProductoLabel.setValue(productoSeleccionado.getValorProducto().toString());
	}

	private void establecerRestricciones() {
		idClienteTextbox.setConstraint(new TextoConstraint(10, "\\d+"));
		nombreTextbox.setConstraint(new TextoConstraint(30, "^[\\w ]+$"));
		emailTextbox.setConstraint(new TextoConstraint(30,
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"));
		direccionTextbox.setConstraint(new TextoConstraint(30, "^[a-zA-Z0-9 ]*$"));
		telefonoTextbox.setConstraint(new TextoConstraint(10, "\\d+"));
		ciudadTextbox.setConstraint(new TextoConstraint(10, "^[\\w ]+$"));
		departamentoTextbox.setConstraint(new TextoConstraint(10, "^[\\w ]+$"));
	}

}
