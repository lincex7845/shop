/**
 * 
 */
package com.mera.shop.controladores;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import com.mera.shop.entidades.Inventario;
import com.mera.shop.entidades.Producto;
import com.mera.shop.servicios.InventarioServicio;
import com.mera.shop.utilidades.SeguridadUtil;

/**
 * Objeto que gestiona las acciones generadas por el usuario en la pagina
 * principal
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Controller
@Qualifier("indexController")
public class IndexController extends GenericForwardComposer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6207204581299663949L;

	/***
	 * referencia al bean/servicio de inventario
	 */
	private InventarioServicio inventarioServicio;
	
	/***
	 * Establece referencia al bean/servicio de inventario
	 * 
	 * @param inventarioServicio
	 */
	@Autowired
	public void setInventarioServicio(InventarioServicio inventarioServicio) {
		this.inventarioServicio = inventarioServicio;
	}

	/***
	 * Sobreescribe metodo doAfterCompose para crear inventario
	 */
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		// Llenar lista de productos
		crearInventario();
	}

	/***
	 * @return retorna todos los elementos del inventario
	 */
	public List<Inventario> getInventario() {
		return inventarioServicio.obtenerTodosElementos();
	}

	/***
	 * Captura eventos re-direccionados del tipo onClick
	 * 
	 * @param e
	 *            Evento accionado por el usuario al momento de seleccionar el
	 *            producto a comprar
	 */
	public void onClick$comprarButton(ForwardEvent e) {
		HttpSession sesion = (HttpSession)Sessions.getCurrent().getNativeSession();
		sesion.setAttribute("producto", inventarioServicio.obtenerElementoPorId(e.getOrigin().getTarget().getId()));
		execution.sendRedirect("checkout.zul");
	}

	/***
	 * Captura eventos re-direccionados del tipo onCreate
	 * 
	 * @param e
	 *            Evento accionado cuando se genera cada boton de compra por
	 *            producto
	 */
	public void onCreate$comprarButton(ForwardEvent e) {
		String idBoton = e.getOrigin().getTarget().getId();
		int cantidadDisponible = inventarioServicio.obtenerElementoPorId(idBoton).getCantidadDisponible();
		Button botonComprar = (Button) Path.getComponent("/mainWindow/" + idBoton);
		if (cantidadDisponible < 1) {
			botonComprar.setDisabled(true);
		}
	}

	/***
	 * Crea los productos en el inventario
	 */
	private void crearInventario() {
		if (inventarioServicio.obtenerTodosElementos().isEmpty()) {
			String[] rutasImagenes = new String[] { "/resources/img/1.png", "/resources/img/2.png",
					"/resources/img/3.png", "/resources/img/4.png" };
			String[] nombresProductos = new String[] { "camisa marca ", "buso marca ", "camiseta marca ",
					"blusa marca " };
			Random randomGenerator = new Random();
			for (int i = 0; i <= nombresProductos.length; i++) {
				int j = randomGenerator.nextInt(nombresProductos.length);
				Producto producto = new Producto(SeguridadUtil.estandarUID(), rutasImagenes[j],
						nombresProductos[j] + (i + 1),
						new BigDecimal(generarPrecioAleatorio(1, nombresProductos.length)));
				inventarioServicio.agregarNuevoElemento(new Inventario(SeguridadUtil.secureUID(), producto, i + j));
			}
		}
	}

	/***
	 * Genera un precio aleatorio
	 * 
	 * @param inicioRango
	 *            Valor minimo del precio a generar
	 * @param finRango
	 *            Valor maximo del precio a generar
	 * @return Precio del producto. El valor se genera en el rango establecido,
	 *         pero se retorna multiplicado por mil
	 */
	private static int generarPrecioAleatorio(int inicioRango, int finRango) {
		if (inicioRango > finRango) {
			throw new IllegalArgumentException("El limite inferior del rango excede el limite del rango");
		}
		Random randomGenerator = new Random();
		long rango = (long) finRango - ((long) inicioRango + 1);
		long fraccion = (long) (rango * randomGenerator.nextDouble());
		return ((int) (fraccion + inicioRango)) * 1000;
	}
}
