/**
 * 
 */
package com.mera.shop.controladores;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.SuspendNotAllowedException;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.mera.shop.entidades.Inventario;
import com.mera.shop.entidades.Transaccion;
import com.mera.shop.entidades.payu.RespuestaConsultaOrdenPayU;
import com.mera.shop.entidades.payu.RespuestaReembolsoPayU;
import com.mera.shop.servicios.InventarioServicio;
import com.mera.shop.servicios.TransaccionServicio;
import com.mera.shop.utilidades.PayuAPIUtil;
import com.mera.shop.utilidades.ValidadorUtil;
import com.payu.sdk.model.OrderStatus;
import com.payu.sdk.model.TransactionState;

/**
 * Objeto que gestiona las acciones generadas por el administrador en la pagina
 * de administracion
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Controller
@Qualifier("adminController")
public class AdminController  extends GenericForwardComposer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7979604755591542836L;
	
	private Window reembolsarDialog;
	
	/***
	 * referencia al bean/servicio de inventario
	 */
	private InventarioServicio inventarioServicio;
	/***
	 * Referencia al servicio de transacciones
	 */
	private TransaccionServicio transaccionServicio;
		
	/***
	 * Establece referencia al bean/servicio de inventario
	 * 
	 * @param inventarioServicio
	 */
	@Autowired
	public void setInventarioServicio(InventarioServicio inventarioServicio) {
		this.inventarioServicio = inventarioServicio;
	}
	
	/***
	 * Establece referencia al bean/servicio de transacciones
	 * 
	 * @param transaccionServicio
	 */
	@Autowired
	public void setTransaccionServicio(TransaccionServicio transaccionServicio) {
		this.transaccionServicio = transaccionServicio;
	}
	
	/***
	 * Sobreescribe metodo doAfterCompose
	 */
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
	}

	/***
	 * @return retorna todos las transacciones
	 */
	public List<Transaccion> getTransacciones() {
		return transaccionServicio.obtenerTodosElementos();
	}
	
	/***
	 * Recibe re-direccionamiento del evento OnCreate para las etiquetas del valor de producto
	 * @param e
	 */
	public void onCreate$valorProductoLabel(ForwardEvent e){
		String idProducto = e.getOrigin().getTarget().getId();
		Label valorProductoLabel = (Label) Path.getComponent("/mainWindow/" + idProducto);
		valorProductoLabel.setValue(inventarioServicio.obtenerElementoPorId(idProducto).getProducto().getValorProducto().toString());
	}
	
	/***
	 * Recibe re-direccionamiento del evento OnCreate para los botones de reembolso
	 * @param e
	 */
	public void onCreate$reembolsarButton(ForwardEvent e){
		String idTransaccion = e.getOrigin().getTarget().getId();
		OrderStatus estadoOrden = OrderStatus.valueOf(transaccionServicio.obtenerElementoPorId(idTransaccion).getStateTransactionPayU());
		Button botonReembolsar = (Button) Path.getComponent("/mainWindow/" + idTransaccion);
		if (!estadoOrden.equals(OrderStatus.CAPTURED)) {
			botonReembolsar.setDisabled(true);
		}
	}
	
	/***
	 * Captura evento de solicitar un reembolso. Se muestra una ventana modal en la que se solicita el motivo
	 * @param e
	 * @throws SuspendNotAllowedException
	 * @throws InterruptedException
	 */
	public void onClick$reembolsarButton(ForwardEvent e) throws SuspendNotAllowedException, InterruptedException{
		HttpSession sesion = (HttpSession)Sessions.getCurrent().getNativeSession();
		sesion.setAttribute("transaccion", transaccionServicio.obtenerElementoPorId(e.getOrigin().getTarget().getId()));
		Window window = (Window) Executions.createComponents("reembolsarDialog.zul", null, null);
		window.doModal();
	}
	
	/***
	 * Captura confirmacion por parte del usuario para iniciar el reembolso
	 * @param e
	 * @throws InterruptedException
	 */
	public void onClick$confirmarReembolsoButton(Event e) throws InterruptedException{
		reembolsarDialog.detach();
		HttpSession sesion = (HttpSession) Sessions.getCurrent().getNativeSession();
		if (sesion.getAttribute("transaccion") == null) {
			Messagebox.show("No se ha seleccionado ninguna transaccion a reembolsar.", "Solicitar Reembolso", Messagebox.OK,
					Messagebox.ERROR);
		}
		else{
			Transaccion transaccion = (Transaccion)sesion.getAttribute("transaccion");
			RespuestaReembolsoPayU respuestaReembolso = PayuAPIUtil.reembolsarOrderPayU(transaccion, "Prueba");
			if(respuestaReembolso != null){
				if(respuestaReembolso.getTransactionResponse().getState().equals(TransactionState.APPROVED)){
					verificarReembolso(transaccion);
				}
				else{
					Messagebox.show(respuestaReembolso.getError(), "Solicitar Reembolso", Messagebox.OK,
							Messagebox.ERROR);
				}
			}
			else{
				Messagebox.show("No es posible conectarse al servidor de pagos para solicitar el reembolso.", "Solicitar Reembolso", Messagebox.OK,
						Messagebox.ERROR);
			}
		}		
	}
	
	/***
	 * Captura evento de cancelar reembolso
	 * @param e
	 */
	public void onClick$cancelarReembolsoButton(Event e){
		reembolsarDialog.detach();
	}
	
	/***
	 * Procesa respuesta a la solicitud de reembolso
	 * @param transaccion	Transaccion sobre la cual se solicita el reembolso
	 * @throws InterruptedException
	 */
	@SuppressWarnings("unchecked")
	private void verificarReembolso(Transaccion transaccion) throws InterruptedException{
		RespuestaConsultaOrdenPayU orderResponse = PayuAPIUtil
				.consultarEstadoOrderPayUPorId(transaccion.getIdOrderPayU());
		if(orderResponse !=null){
			if(!ValidadorUtil.isTextoValido(orderResponse.getError())){
				Map<String, Object> datosOrden = (HashMap<String, Object>) orderResponse
						.getResult().getPayload();
				OrderStatus estadoOrden = OrderStatus.valueOf((String) datosOrden
						.get("status"));
				if(estadoOrden == OrderStatus.REFUNDED){
					// Aumentar inventario
					Inventario inventario = inventarioServicio.obtenerElementoPorId(transaccion.getIdProducto());
					inventario.setCantidadDisponible(inventario.getCantidadDisponible()+1);
					inventarioServicio.actualizarElemento(inventario);
					// Informar usuario
					Messagebox.show("La solicitud de reembolso se aprobo satisfactoriamente.", "Solicitar Reembolso", Messagebox.OK,
							Messagebox.INFORMATION);
				}
				else if(estadoOrden.equals(OrderStatus.IN_PROGRESS)){
					Messagebox.show("La solicitud de reembolso esta pendiente de aprobacion por parte de la entidad financiera.", "Solicitar Reembolso", Messagebox.OK,
							Messagebox.INFORMATION);
				}
				else{
					Messagebox.show("Ocurri� un error al solicitar el reembolso.", "Solicitar Reembolso", Messagebox.OK,
							Messagebox.ERROR);
				}
			}
			else{
				Messagebox.show(orderResponse.getError(), "Solicitar Reembolso", Messagebox.OK,
						Messagebox.ERROR);
			}
		}
		else{
			Messagebox.show("No es posible conectarse al servidor de pagos para confirmar el reembolso.", "Solicitar Reembolso", Messagebox.OK,
					Messagebox.ERROR);
		}
	}
}
