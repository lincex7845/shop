/**
 * 
 */
package com.mera.shop.servicios;

import java.util.List;

/**
 * Interfaz que describe funcionalidades para la gestion de entidades
 * en los objetos de la capa de servicios
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public interface IServicio<T> {
	
	/***
	 * Definicion abstracta para agregar una nueva entidad de datos en la capa de servicios
	 * @param nuevoElemento	Elemento a agregar
	 */
	public void agregarNuevoElemento(T nuevoElemento);
	
	/***
	 * Definicion abstracta para obtener una entidad de datos por su identificador
	 * @param idElemento	Identificador del elemento
	 */
	public T obtenerElementoPorId(String idElemento);
	
	/***
	 * Definicion abstracta para obtener todas las instancias de una entidad de datos
	 */
	public List<T> obtenerTodosElementos();
	
	/***
	 * Definicion abstracta para actualizar una entidad de datos en la capa de servicios
	 * @param elementoActualizado	Elemento a actualizar
	 */
	public void actualizarElemento(T elementoActualizado);
	
	/***
	 * Definicion abstracta para eliminar una entidad de datos en la capa de servicios
	 * @param idElemento	Identificador del elemento
	 */
	public void eliminarElemento(String idElemento);
}
