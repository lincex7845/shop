/**
 * 
 */
package com.mera.shop.servicios;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.mera.shop.daos.TransaccionDAO;
import com.mera.shop.entidades.Transaccion;

/**
 * Objeto que sirve funcionalidades para la gestion de transacciones/pagos con PayU
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Service
@Qualifier("transaccionesServicio")
public class TransaccionServicio implements IServicio<Transaccion> {

	@Resource
	private TransaccionDAO transaccionesRepositorio;
	
	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#agregarNuevoElemento(java.lang.Object)
	 */
	@Override
	public void agregarNuevoElemento(Transaccion nuevoElemento) {
		transaccionesRepositorio.agregarElemento(nuevoElemento);
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#obtenerElementoPorId(java.lang.String)
	 */
	@Override
	public Transaccion obtenerElementoPorId(String idElemento) {
		return transaccionesRepositorio.buscarElementoPorId(idElemento);
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#obtenerTodosElementos()
	 */
	@Override
	public List<Transaccion> obtenerTodosElementos() {
		return transaccionesRepositorio.obtenerTodosElementos();
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#actualizarElemento(java.lang.Object)
	 */
	@Override
	public void actualizarElemento(Transaccion elementoActualizado) {
		transaccionesRepositorio.actualizarElemento(elementoActualizado);

	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#eliminarElemento(java.lang.String)
	 */
	@Override
	public void eliminarElemento(String idElemento) {
		transaccionesRepositorio.eliminarElemento(idElemento);
	}

}
