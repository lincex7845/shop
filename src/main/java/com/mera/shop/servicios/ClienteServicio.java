/**
 * 
 */
package com.mera.shop.servicios;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.mera.shop.daos.ClienteDAO;
import com.mera.shop.entidades.Cliente;

/**
 * Objeto que sirve funcionalidades para la gestion de clientes
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Service
@Qualifier("clientesServicio")
public class ClienteServicio implements IServicio<Cliente> {
	
	@Resource
	private ClienteDAO clientesRepositorio;
	
	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#agregarNuevoElemento(java.lang.Object)
	 */
	@Override
	public void agregarNuevoElemento(Cliente nuevoElemento) {
		clientesRepositorio.agregarElemento(nuevoElemento);
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#obtenerElementoPorId(java.lang.String)
	 */
	@Override
	public Cliente obtenerElementoPorId(String idElemento) {
		return clientesRepositorio.buscarElementoPorId(idElemento);
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#obtenerTodosElementos()
	 */
	@Override
	public List<Cliente> obtenerTodosElementos() {
		return clientesRepositorio.obtenerTodosElementos();
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#actualizarElemento(java.lang.Object)
	 */
	@Override
	public void actualizarElemento(Cliente elementoActualizado) {
		clientesRepositorio.actualizarElemento(elementoActualizado);
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#eliminarElemento(java.lang.String)
	 */
	@Override
	public void eliminarElemento(String idElemento) {
		clientesRepositorio.eliminarElemento(idElemento);
	}

}
