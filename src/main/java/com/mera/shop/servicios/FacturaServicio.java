/**
 * 
 */
package com.mera.shop.servicios;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.mera.shop.daos.FacturaDAO;
import com.mera.shop.entidades.Factura;

/**
 * Objeto que sirve funcionalidades para la gestion de facturas
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Service
@Qualifier("facturasServicio")
public class FacturaServicio implements IServicio<Factura> {

	@Resource
	private FacturaDAO facturasRepositorio;
	
	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#agregarNuevoElemento(java.lang.Object)
	 */
	@Override
	public void agregarNuevoElemento(Factura nuevoElemento) {
		facturasRepositorio.agregarElemento(nuevoElemento);

	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#obtenerElementoPorId(java.lang.String)
	 */
	@Override
	public Factura obtenerElementoPorId(String idElemento) {
		return facturasRepositorio.buscarElementoPorId(idElemento);
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#obtenerTodosElementos()
	 */
	@Override
	public List<Factura> obtenerTodosElementos() {
		return facturasRepositorio.obtenerTodosElementos();
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#actualizarElemento(java.lang.Object)
	 */
	@Override
	public void actualizarElemento(Factura elementoActualizado) {
		facturasRepositorio.actualizarElemento(elementoActualizado);
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#eliminarElemento(java.lang.String)
	 */
	@Override
	public void eliminarElemento(String idElemento) {
		facturasRepositorio.eliminarElemento(idElemento);

	}

}
