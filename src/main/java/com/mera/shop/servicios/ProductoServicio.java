/**
 * 
 */
package com.mera.shop.servicios;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.mera.shop.daos.ProductoDAO;
import com.mera.shop.entidades.Producto;

/**
 * Objeto que sirve funcionalidades para la gestion de productos
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Service
@Qualifier("productosServicio")
public class ProductoServicio implements IServicio<Producto> {

	@Resource
	private ProductoDAO productosRepositorio;
		
	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#agregarNuevoElemento(java.lang.Object)
	 */
	@Override
	public void agregarNuevoElemento(Producto nuevoElemento) {
		productosRepositorio.agregarElemento(nuevoElemento);

	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#obtenerElementoPorId(java.lang.String)
	 */
	@Override
	public Producto obtenerElementoPorId(String idElemento) {
		return productosRepositorio.buscarElementoPorId(idElemento);
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#obtenerTodosElementos()
	 */
	@Override
	public List<Producto> obtenerTodosElementos() {
		return productosRepositorio.obtenerTodosElementos();
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#actualizarElemento(java.lang.Object)
	 */
	@Override
	public void actualizarElemento(Producto elementoActualizado) {
		productosRepositorio.actualizarElemento(elementoActualizado);

	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#eliminarElemento(java.lang.String)
	 */
	@Override
	public void eliminarElemento(String idElemento) {
		productosRepositorio.eliminarElemento(idElemento);
	}

}
