/**
 * 
 */
package com.mera.shop.servicios;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.mera.shop.daos.InventarioDAO;
import com.mera.shop.entidades.Inventario;

/**
 * Objeto que sirve funcionalidades para la gestion del inventario
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Service
@Qualifier("inventarioServicio")
public class InventarioServicio implements IServicio<Inventario> {

	@Resource
	private InventarioDAO inventarioRepositorio;
	
	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#agregarNuevoElemento(java.lang.Object)
	 */
	@Override
	public void agregarNuevoElemento(Inventario nuevoElemento) {
		inventarioRepositorio.agregarElemento(nuevoElemento);
	}

	/**
	 * Permite obtener un registro del inventario dado el identificador del producto
	 * @param idProducto Identificador del producto
	 * @see com.mera.shop.servicios.IServicio#obtenerElementoPorId(java.lang.String)
	 */
	@Override
	public Inventario obtenerElementoPorId(String idProducto) {
		return inventarioRepositorio.buscarElementoPorId(idProducto);
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#obtenerTodosElementos()
	 */
	@Override
	public List<Inventario> obtenerTodosElementos() {
		return inventarioRepositorio.obtenerTodosElementos();
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.servicios.IServicio#actualizarElemento(java.lang.Object)
	 */
	@Override
	public void actualizarElemento(Inventario elementoActualizado) {
		inventarioRepositorio.actualizarElemento(elementoActualizado);

	}

	/**
	 * Permite eliminar un registro del inventario
	 * @param idInventario Identificador del producto en el inventario
	 * @see com.mera.shop.servicios.IServicio#eliminarElemento(java.lang.String)
	 */
	@Override
	public void eliminarElemento(String idInventario) {
		inventarioRepositorio.eliminarElemento(idInventario);

	}

}
