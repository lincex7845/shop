/**
 * 
 */
package com.mera.shop.servicios;

import java.util.HashMap;
import java.util.Map;
import org.jboss.logging.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.mera.shop.entidades.Cliente;
import com.mera.shop.entidades.PagoTarjetaCredito;
import com.mera.shop.entidades.Producto;
import com.mera.shop.entidades.Transaccion;
import com.mera.shop.entidades.payu.RespuestaConsultaOrdenPayU;
import com.mera.shop.utilidades.PayuAPIUtil;
import com.mera.shop.utilidades.PayuSDKUtil;
import com.mera.shop.utilidades.ValidadorUtil;
import com.payu.sdk.model.OrderStatus;
import com.payu.sdk.model.TransactionResponse;
import com.payu.sdk.model.TransactionState;

/**
 * Objeto que sirve funcionalidades para la gestion de pagos, consumiendo PayU
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-10
 * @version 1
 */
@Service
@Qualifier("payuServicio")
public class PagosServicio {

	private static TransaccionServicio transaccionesServicio;

	private static final Logger LOGGER = Logger.getLogger(PagosServicio.class.getName());

	@Autowired
	public void setTransaccionesServicio(TransaccionServicio transaccionesServicio) {
		PagosServicio.transaccionesServicio = transaccionesServicio;
	}

	/***
	 * Permite realizar el pago a traves de PayU
	 * 
	 * @param transaccion
	 *            Objeto que contiene informacion adicional sobre la compra
	 * @param producto
	 *            Objeto que contiene informacion sobre el producto a pagar
	 * @param cliente
	 *            Objeto que contiene informacion sobre el cliente que realiza
	 *            el pago
	 * @param pago
	 *            Objeto que contiene informacion sobre el medio de pago
	 * @return Verdadero si el pago se lleva a cabo satisfactoriamente, falso si
	 *         ocurre un error o el pago es declinado
	 */
	public static boolean realizarPago(Transaccion transaccion, Producto producto, Cliente cliente,
			PagoTarjetaCredito pago) {
		boolean resultado = false;
		TransactionResponse resultadoTransaccion = PayuSDKUtil.ejecutarPago(transaccion, producto, cliente, pago);
		if (resultadoTransaccion != null && resultadoTransaccion.getOrderId() != null) {
			transaccion.setIdOrderPayU(resultadoTransaccion.getOrderId().toString());
			transaccion.setIdTransactionPayU(resultadoTransaccion.getTransactionId());
			transaccionesServicio.actualizarElemento(transaccion);
			resultado = resultadoTransaccion.getState() != TransactionState.DECLINED
					&& resultadoTransaccion.getState() != TransactionState.ERROR;
			if (!resultado) {
				LOGGER.error(transaccion.getIdOrderPayU() + " " + resultadoTransaccion.getErrorCode() + " "
						+ resultadoTransaccion.getPaymentNetworkResponseErrorMessage());
			}
		}
		return resultado;
	}

	/***
	 * Metodo para consultar el estado de una orden y la transaccion
	 * 
	 * @param transaccion
	 *            Objeto que contiene informacion sobre la orden y la
	 *            transaccion en PayU
	 * @return El estado de la orden en PayU
	 */
	@SuppressWarnings("unchecked")
	public static OrderStatus consultarEstadoPago(Transaccion transaccion) {
		RespuestaConsultaOrdenPayU resultadoConsultaOrden = PayuAPIUtil
				.consultarEstadoOrderPayUPorId(transaccion.getIdOrderPayU());
		if (resultadoConsultaOrden != null && !ValidadorUtil.isTextoValido(resultadoConsultaOrden.getError())) {
			Map<String, Object> datosOrden = (HashMap<String, Object>) resultadoConsultaOrden.getResult().getPayload();
			OrderStatus estadoOrden = OrderStatus.valueOf((String) datosOrden.get("status"));
			transaccion.setStateTransactionPayU(estadoOrden.name());
			transaccionesServicio.actualizarElemento(transaccion);
			return estadoOrden;
		} else if (ValidadorUtil.isTextoValido(resultadoConsultaOrden.getError())) {
			LOGGER.error(transaccion.getIdOrderPayU() + " " + resultadoConsultaOrden.getError());
		}
		return null;
	}

	/***
	 * Metodo para efectuar un reembolso. Se requiere que la transaccion sea
	 * aprobada (Estado CAPTURED).
	 * 
	 * @param transaccion
	 *            Objeto que contiene informacion sobre la orden y la
	 *            transaccion en PayU
	 * @param razonParaReembolso
	 *            Descripcion del motivo por el que se solicita el reembolso
	 * @return Verdadero si el reembolso se lleva a cabo satisfactoriamente,
	 *         falso si ocurre un error
	 * @throws IllegalArgumentException
	 *             Si la transaccion no esta Aprobada
	 */
	public static boolean realizarReembolsoSDK(Transaccion transaccion, String razonParaReembolso) {
		boolean resultado = false;
		if (transaccion.getStateTransactionPayU().equals(OrderStatus.CAPTURED.name())) {
			TransactionResponse resultadoTransaccion = PayuSDKUtil.ejecutarReembolso(transaccion, razonParaReembolso);
			if (validarRespuestaTransaccion(resultadoTransaccion)) {
				OrderStatus estadoOrden = consultarEstadoPago(transaccion);
				transaccion.setStateTransactionPayU(estadoOrden.name());
				transaccionesServicio.actualizarElemento(transaccion);
				resultado = estadoOrden == OrderStatus.REFUNDED;
				if (!resultado) {
					LOGGER.error(transaccion.getIdOrderPayU() + " " + estadoOrden.name());
				}
			} else {
				LOGGER.error(transaccion.getIdOrderPayU() + " " + resultadoTransaccion.getErrorCode() + " "
						+ resultadoTransaccion.getPaymentNetworkResponseErrorMessage());
			}
		} else {
			throw new IllegalArgumentException(
					"El estado de la orden en PayU es " + transaccion.getStateTransactionPayU());
		}
		return resultado;
	}

	/***
	 * Valida si la transaccion con PayU se realizo satisfactoriamente
	 * 
	 * @param resultadoTransaccion
	 *            Objeto respuesta obtenido en la transaccion
	 * @return Resultado de la validacion. Se verifica si la respuesta no es
	 *         nula, y si el estado de la transaccion es APROBADA
	 */
	private static boolean validarRespuestaTransaccion(TransactionResponse resultadoTransaccion) {
		return resultadoTransaccion != null && resultadoTransaccion.getOrderId() != null
				&& resultadoTransaccion.getState() == TransactionState.APPROVED;
	}

}
