/**
 * 
 */
package com.mera.shop.entidades.payu;

import com.payu.sdk.reporting.model.ReportingResult;

/**
 * POJO para recibir respuesta de consultas del API payU
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-9
 * @version 1
 */
public class RespuestaConsultaOrdenPayU {

	private String code;
	private String error;
	private ReportingResult result;
	
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}
	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}
	/**
	 * @return the result
	 */
	public ReportingResult getResult() {
		return result;
	}
	/**
	 * @param result the result to set
	 */
	public void setResult(ReportingResult result) {
		this.result = result;
	}
}
