/**
 * 
 */
package com.mera.shop.entidades.payu;

import com.payu.sdk.model.Merchant;
import com.payu.sdk.model.Transaction;

/**
 * POJO para ejecutar peticiones de reembolso al API PayU
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-9
 * @version 1
 */
public class PeticionReembolsoPayU {

	/***
	 * Indica si la peticion es de pruebas
	 */
	private boolean test;
	/***
	 * Lenguage soportado para la respuesta
	 */
	private String language;
	/**
	 * Indica la operacion a realizar
	 */
	private String command;
	/**
	 * Credenciales del comercio
	 */
	private Merchant merchant;
	/***
	 * Datos de la transaccion a reembolsar
	 */
	private Transaction transaction;
	
	/**
	 * @return the test
	 */
	public boolean isTest() {
		return test;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @return the command
	 */
	public String getCommand() {
		return command;
	}
	/**
	 * @return the merchant
	 */
	public Merchant getMerchant() {
		return merchant;
	}
	/**
	 * @return the transaction
	 */
	public Transaction getTransaction() {
		return transaction;
	}
	/**
	 * @param test the test to set
	 */
	public void setTest(boolean test) {
		this.test = test;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * @param command the command to set
	 */
	public void setCommand(String command) {
		this.command = command;
	}
	/**
	 * @param merchant the merchant to set
	 */
	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}
	/**
	 * @param transaction the transaction to set
	 */
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
}
