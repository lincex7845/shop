/**
 * 
 */
package com.mera.shop.entidades.payu;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.payu.sdk.model.TransactionResponse;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * POJO para recibir respuesta de reembolsos del API payU
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-9
 * @version 1
 */
@JsonInclude(Include.NON_NULL)
public class RespuestaReembolsoPayU {
	private String code;
	private String error;
	private TransactionResponse transactionResponse;
	
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @return the error
	 */
	public String getError() {
		return error;
	}
	/**
	 * @return the transactionResponse
	 */
	public TransactionResponse getTransactionResponse() {
		return transactionResponse;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @param error the error to set
	 */
	public void setError(String error) {
		this.error = error;
	}
	/**
	 * @param transactionResponse the transactionResponse to set
	 */
	public void setTransactionResponse(TransactionResponse transactionResponse) {
		this.transactionResponse = transactionResponse;
	}
}
