package com.mera.shop.entidades.payu;

import java.util.Map;

import com.payu.sdk.model.Merchant;

/**
 * POJO para ejecutar peticiones de consulta al API PayU
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-9
 * @version 1
 */
@SuppressWarnings("rawtypes")
public class PeticionConsultaPayU {

	/***
	 * Indica si la peticion es de pruebas
	 */
	private boolean test;
	/***
	 * Lenguage soportado para la respuesta
	 */
	private String language;
	/**
	 * Indica la operacion a realizar
	 */
	private String command;
	/**
	 * Credenciales del comercio
	 */
	private Merchant merchant;
	/***
	 * Parametros a consultar
	 */
	private Map details;
	
	/***
	 * Constructor por defecto
	 */
	public PeticionConsultaPayU(){
		// constructor
	}
		
	/***
	 * constructor
	 * @param test		Bandera para determinar si la peticion es un test
	 * @param language	Indica el lenguaje a soportar en la respuesta
	 * @param command	Operacion a realizar contra la API PayU
	 * @param merchant	Credenciales del comercio
	 * @param details	Argumentos de la consulta
	 */
	public PeticionConsultaPayU(boolean test, String language, String command, Merchant merchant, Map details){
		this.test = test;
		this.language = language;
		this.command = command;
		this.merchant = merchant;
		this.details = details;
	}
	
	/**
	 * @return the test
	 */
	public boolean isTest() {
		return test;
	}
	/**
	 * @param test the test to set
	 */
	public void setTest(boolean test) {
		this.test = test;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * @return the command
	 */
	public String getCommand() {
		return command;
	}
	/**
	 * @param command the command to set
	 */
	public void setCommand(String command) {
		this.command = command;
	}
	/**
	 * @return the merchant
	 */
	public Merchant getMerchant() {
		return merchant;
	}
	/**
	 * @param merchant the merchant to set
	 */
	public void setMerchant(Merchant merchant) {
		this.merchant = merchant;
	}
	/**
	 * @return the details
	 */
	public Map getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(Map details) {
		this.details = details;
	}
}
