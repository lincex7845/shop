/**
 * 
 */
package com.mera.shop.entidades;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Esta clase se usa para representar la factura asociada a la compra de un cliente
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Entity
public class Factura {
	
	/***
	 * Identificador de la factura
	 */
	@Id
	@NotEmpty
	private String idFactura;
	/***
	 * Fecha en la que se factura la venta
	 */
	@NotNull
	private Date fechaFactura;
	/**
	 * Identificador del cliente al que se emite la factura
	 */
	@NotEmpty
	private String idCliente;
	/***
	 * Identificador del producto vendido
	 */
	@NotEmpty
	private String idProducto;
	
	/***
	 * Describe la razon por la que se emite la factura
	 */
	@NotEmpty
	private String conceptoFactura;
	
	/**
	 * Constructor
	 * @param idFactura		Identificador de la factura
	 * @param fechaFactura	Fecha de emision de la factura
	 * @param idCliente		Identificador del cliente al quien se emite la factura
	 * @param idProducto	Identificador del producto vendido
	 * @param conceptoFactura Describe la razon por la que se emite la factura
	 */
	public Factura(String idFactura, Date fechaFactura, String idCliente,
			String idProducto, String conceptoFactura) {
		this.idFactura = idFactura;
		this.fechaFactura = fechaFactura;
		this.idCliente = idCliente;
		this.idProducto = idProducto;
		this.conceptoFactura = conceptoFactura;
	}

	/**
	 * @return the idFactura
	 */
	public String getIdFactura() {
		return idFactura;
	}

	/**
	 * @return the fechaFactura
	 */
	public Date getFechaFactura() {
		return fechaFactura;
	}

	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}

	/**
	 * @return the idProducto
	 */
	public String getIdProducto() {
		return idProducto;
	}

	/**
	 * @return the conceptoFactura
	 */
	public String getConceptoFactura() {
		return conceptoFactura;
	}

	/**
	 * @param idFactura the idFactura to set
	 */
	public void setIdFactura(String idFactura) {
		this.idFactura = idFactura;
	}

	/**
	 * @param fechaFactura the fechaFactura to set
	 */
	public void setFechaFactura(Date fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	/**
	 * @param conceptoFactura the conceptoFactura to set
	 */
	public void setConceptoFactura(String conceptoFactura) {
		this.conceptoFactura = conceptoFactura;
	}
		
}