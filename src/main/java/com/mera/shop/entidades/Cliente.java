/**
 * 
 */
package com.mera.shop.entidades;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Entidad usada para representar un comprador
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Entity
public class Cliente {

	/***
	 * Identificador del Cliente
	 */
	@Id
	@NotEmpty
	private String idCliente;
	/***
	 * Nombre completo del cliente
	 */
	@NotEmpty
	private String nombreCliente;
	/***
	 * Correo electronico del cliente
	 */
	@NotEmpty
	private String correoCliente;
	/***
	 * Telefono de contacto del cliente
	 */
	@NotEmpty
	private String telefonoCliente;
	/***
	 * Direccion para contacto o envio el cliente
	 */
	
	/***
	 * Lugar de residencia del cliente
	 */
	@NotNull
	private Residencia residencia;
		
	/**
	 * Constructor
	 * @param idCliente			Identificacion del cliente
	 * @param nombreCliente		Nombre completo cliente
	 * @param correoCliente		Correo electronico
	 * @param telefonoCliente	Telefono cliente
	 * @param direccion			Lugar de residencia del cliente
	 */
	public Cliente(String idCliente, String nombreCliente,
			String correoCliente, String telefonoCliente,			
			Residencia residencia) {
		this.idCliente = idCliente;
		this.nombreCliente = nombreCliente;
		this.correoCliente = correoCliente;
		this.telefonoCliente = telefonoCliente;
		this.residencia = residencia;
	}

	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}

	/**
	 * @return the nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}

	/**
	 * @return the correoCliente
	 */
	public String getCorreoCliente() {
		return correoCliente;
	}

	/**
	 * @return the telefonoCliente
	 */
	public String getTelefonoCliente() {
		return telefonoCliente;
	}

	/**
	 * @return the residencia
	 */
	public Residencia getResidencia() {
		return residencia;
	}

	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	/**
	 * @param nombreCliente the nombreCliente to set
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	/**
	 * @param correoCliente the correoCliente to set
	 */
	public void setCorreoCliente(String correoCliente) {
		this.correoCliente = correoCliente;
	}

	/**
	 * @param telefonoCliente the telefonoCliente to set
	 */
	public void setTelefonoCliente(String telefonoCliente) {
		this.telefonoCliente = telefonoCliente;
	}

	/**
	 * @param residencia the residencia to set
	 */
	public void setResidencia(Residencia residencia) {
		this.residencia = residencia;
	}
	
	
}
