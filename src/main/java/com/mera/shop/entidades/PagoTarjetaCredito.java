/**
 * 
 */
package com.mera.shop.entidades;

import com.mera.shop.utilidades.FranquiciaTarjetaCredito;

/**
 * Entidad usada para representar un pago efectuado con tarjeta de credito. 
 * No se almacena
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public class PagoTarjetaCredito {

	private String numero;
	private int anioExpiracion;
	private int mesExpiracion;
	private int codigoSeguridad;
	private int numeroCoutas;
	private FranquiciaTarjetaCredito franquicia;
			
	/**
	 * @param numero
	 * @param anioExpiracion
	 * @param mesExpiracion
	 * @param codigoSeguridad
	 * @param franquicia
	 */
	public PagoTarjetaCredito(String numero, int anioExpiracion, int mesExpiracion, int codigoSeguridad,
			FranquiciaTarjetaCredito franquicia, int numeroCoutas) {
		this.numero = numero;
		this.anioExpiracion = anioExpiracion;
		this.mesExpiracion = mesExpiracion;
		this.codigoSeguridad = codigoSeguridad;
		this.franquicia = franquicia;
		this.numeroCoutas = numeroCoutas;
	}
	
	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}
	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	/**
	 * @return the anioExpiracion
	 */
	public int getAnioExpiracion() {
		return anioExpiracion;
	}
	/**
	 * @param anioExpiracion the anioExpiracion to set
	 */
	public void setAnioExpiracion(int anioExpiracion) {
		this.anioExpiracion = anioExpiracion;
	}
	/**
	 * @return the mesExpiracion
	 */
	public int getMesExpiracion() {
		return mesExpiracion;
	}
	/**
	 * @param mesExpiracion the mesExpiracion to set
	 */
	public void setMesExpiracion(int mesExpiracion) {
		this.mesExpiracion = mesExpiracion;
	}
	/**
	 * @return the codigoSeguridad
	 */
	public int getCodigoSeguridad() {
		return codigoSeguridad;
	}
	/**
	 * @param codigoSeguridad the codigoSeguridad to set
	 */
	public void setCodigoSeguridad(int codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}
	/**
	 * @return the franquicia
	 */
	public FranquiciaTarjetaCredito getFranquicia() {
		return franquicia;
	}
	/**
	 * @param franquicia the franquicia to set
	 */
	public void setFranquicia(FranquiciaTarjetaCredito franquicia) {
		this.franquicia = franquicia;
	}

	/**
	 * @return the numeroCoutas
	 */
	public int getNumeroCoutas() {
		return numeroCoutas;
	}

	/**
	 * @param numeroCoutas the numeroCoutas to set
	 */
	public void setNumeroCoutas(int numeroCoutas) {
		this.numeroCoutas = numeroCoutas;
	}
}
