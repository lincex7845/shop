/**
 * 
 */
package com.mera.shop.entidades;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Entidad usada para representar direccion fisica de residencia
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public class Residencia {
	
	@NotEmpty
	private String direccionCliente;
	/**
	 * Codigo postal del lugar de residencia del cliente
	 */
	@NotEmpty
	private String codigoPostalCliente;
	/**
	 * Ciudad de residencia del cliente
	 */
	@NotEmpty
	private String ciudadCliente;
	/***
	 * Estado donde se ubica la ciudad del cliente
	 */
	@NotEmpty
	private String estadoCliente;
	/***
	 * Pais residencia del cliente
	 */
	@NotEmpty
	private String paisCliente;
			
	/**
	 * @param direccionCliente
	 * @param codigoPostalCliente
	 * @param ciudadCliente
	 * @param estadoCliente
	 * @param paisCliente
	 */
	public Residencia(String direccionCliente, String codigoPostalCliente, String ciudadCliente, String estadoCliente,
			String paisCliente) {
		this.direccionCliente = direccionCliente;
		this.codigoPostalCliente = codigoPostalCliente;
		this.ciudadCliente = ciudadCliente;
		this.estadoCliente = estadoCliente;
		this.paisCliente = paisCliente;
	}
	/**
	 * @return the direccionCliente
	 */
	public String getDireccionCliente() {
		return direccionCliente;
	}
	/**
	 * @param direccionCliente the direccionCliente to set
	 */
	public void setDireccionCliente(String direccionCliente) {
		this.direccionCliente = direccionCliente;
	}
	/**
	 * @return the codigoPostalCliente
	 */
	public String getCodigoPostalCliente() {
		return codigoPostalCliente;
	}
	/**
	 * @param codigoPostalCliente the codigoPostalCliente to set
	 */
	public void setCodigoPostalCliente(String codigoPostalCliente) {
		this.codigoPostalCliente = codigoPostalCliente;
	}
	/**
	 * @return the ciudadCliente
	 */
	public String getCiudadCliente() {
		return ciudadCliente;
	}
	/**
	 * @param ciudadCliente the ciudadCliente to set
	 */
	public void setCiudadCliente(String ciudadCliente) {
		this.ciudadCliente = ciudadCliente;
	}
	/**
	 * @return the estadoCliente
	 */
	public String getEstadoCliente() {
		return estadoCliente;
	}
	/**
	 * @param estadoCliente the estadoCliente to set
	 */
	public void setEstadoCliente(String estadoCliente) {
		this.estadoCliente = estadoCliente;
	}
	/**
	 * @return the paisCliente
	 */
	public String getPaisCliente() {
		return paisCliente;
	}
	/**
	 * @param paisCliente the paisCliente to set
	 */
	public void setPaisCliente(String paisCliente) {
		this.paisCliente = paisCliente;
	}
}
