/**
 * 
 */
package com.mera.shop.entidades;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Esta clase se usa para representar el inventario de productos disponibles
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Entity
public class Inventario  {
	
	@Id
	@NotEmpty
	private String idInventario;
	
	
	/***
	 * Identificador del producto en inventario
	 */
	@NotNull
	private Producto producto;
	/***
	 * Indica si esta disponible o no en el inventario
	 */
	private int cantidadDisponible;
	
	
	/**
	 * Constructor	 * 
	 * @param producto		Producto a registrar 
	 * @param disponible	Banderas que indica si est� disponible para la venta
	 * @param idInventario	Identificador para el registro
	 */
	@Valid
	public Inventario(String idInventario, Producto producto, int cantidadDisponible) {
		this.idInventario = idInventario;
		this.producto = producto;
		this.cantidadDisponible = cantidadDisponible;
	}

	/**
	 * @return the idProducto
	 */
	public Producto getProducto() {
		return producto;
	}

	/**
	 * @param idProducto the idProducto to set
	 */
	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	/**
	 * @return the cantidadDisponible
	 */
	public int getCantidadDisponible() {
		return cantidadDisponible;
	}

	/**
	 * @param cantidadDisponible the cantidadDisponible to set
	 */
	public void setCantidadDisponible(int cantidadDisponible) {
		this.cantidadDisponible = cantidadDisponible;
	}

	/**
	 * @return the idInventario
	 */
	public String getIdInventario() {
		return idInventario;
	}

	/**
	 * @param idInventario the idInventario to set
	 */
	public void setIdInventario(String idInventario) {
		this.idInventario = idInventario;
	}

}