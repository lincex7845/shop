/**
 * 
 */
package com.mera.shop.entidades;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Esta clase se usa para representar un producto en venta
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Entity
public class Producto {
	
	/***
	 * Identificador del producto
	 */
	@Id
	@NotEmpty
	private String idProducto;
	/***
	 * Almacena la ruta que 
	 */
	@NotEmpty
	private String rutaImagenProducto;
	/***
	 * Texto que especifica el nombre del producto
	 */
	@NotEmpty
	private String descripcionProducto;
	/***
	 * Cantidad en la cual se vende el producto
	 */
	@NotEmpty
	private BigDecimal valorProducto;
	
	
	/**
	 * Constructor
	 * @param idProducto			Identificador del producto
	 * @param rutaImagenProducto	Ruta en disco de la imagen correspondiente al producto
	 * @param descripcionProducto	Texto que describe el nombre del producto a vender
	 * @param valorProducto			Cantidad de dinero en la que se vende el producto
	 */
	@Valid
	public Producto(String idProducto, String rutaImagenProducto,
			String descripcionProducto, BigDecimal valorProducto) {
		this.idProducto = idProducto;
		this.rutaImagenProducto = rutaImagenProducto;
		this.descripcionProducto = descripcionProducto;
		this.valorProducto = valorProducto;
	}
	
	/**
	 * @return the idProducto
	 */
	public String getIdProducto() {
		return idProducto;
	}
	/**
	 * @return the rutaImagenProducto
	 */
	public String getRutaImagenProducto() {
		return rutaImagenProducto;
	}
	/**
	 * @return the descripcionProducto
	 */
	public String getDescripcionProducto() {
		return descripcionProducto;
	}
	/**
	 * @return the valorProducto
	 */
	public BigDecimal getValorProducto() {
		return valorProducto;
	}
	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	/**
	 * @param rutaImagenProducto the rutaImagenProducto to set
	 */
	public void setRutaImagenProducto(String rutaImagenProducto) {
		this.rutaImagenProducto = rutaImagenProducto;
	}
	/**
	 * @param descripcionProducto the descripcionProducto to set
	 */
	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}
	/**
	 * @param valorProducto the valorProducto to set
	 */
	public void setValorProducto(BigDecimal valorProducto) {
		this.valorProducto = valorProducto;
	}	
}