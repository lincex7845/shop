/**
 * 
 */
package com.mera.shop.entidades;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Entidad usada para representar una transaccion de compra
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Entity
public class Transaccion {
	
	/***
	 * Identificador de la transaccion
	 */
	@Id
	@NotEmpty
	private String idTransaccion;
	/***
	 * Identificador del Producto asociado a la transaccion
	 */
	@NotEmpty
	private String idProducto;
	/***
	 * Identificador del Cliente asociado a la transaccion
	 */
	@NotEmpty
	private String idCliente;
	/**
	 * Identificador del dispositivo donde se realiza la transaccion
	 */
	@NotEmpty
	private String deviceSessionId;
	/***
	 * Direccion IP desde donde se realiza la transaccion
	 */
	@NotEmpty
	private String ipAddress;
	/***
	 * Cookie para identificar sesion que realiza la transaccion
	 */
	@NotEmpty
	private String cookie;
	/***
	 * Navegador desde el cual se realiza la transaccion
	 */
	@NotEmpty
	private String userAgentBrowser;
	/***
	 * Fecha de la transaccion
	 */
	private Date fechaTransaccion;
	/***
	 * Identificador de la orden de servicio en PayU
	 */
	private String idOrderPayU;
	/***
	 * Identificador de la transaccion asociada a la orden en PayU
	 */
	private String idTransactionPayU;
	/**
	 * Estado de la transaccion en PayU
	 */
	private String stateTransactionPayU;
		
	
	/**
	 * Constructor
	 * @param idTransaccion		Identificador transaccion
	 * @param idProducto		Identificador del producto
	 * @param idCliente			Identificador del Cliente
	 * @param deviceSessionId	Identificador del dispositivo del cliente
	 * @param ipAddress			Direccion ip del cliente
	 * @param cookie			Cookie de sesion en el cliente
	 * @param userAgentBrowser	Navegador del cliente
	 */
	public Transaccion(String idTransaccion, String idProducto, String idCliente,
			String deviceSessionId, String ipAddress, String cookie,
			String userAgentBrowser){
		this.idTransaccion = idTransaccion;
		this.idProducto = idProducto;
		this.idCliente = idCliente;
		this.deviceSessionId = deviceSessionId;
		this.ipAddress = ipAddress;
		this.cookie = cookie;
		this.userAgentBrowser = userAgentBrowser;
		this.idOrderPayU = null;
		this.idTransactionPayU = null;
		this.stateTransactionPayU = null;
		this.fechaTransaccion = new Date();
	}

	/**
	 * @return the idTransaccion
	 */
	public String getIdTransaccion() {
		return idTransaccion;
	}

	/**
	 * @return the deviceSessionId
	 */
	public String getDeviceSessionId() {
		return deviceSessionId;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @return the cookie
	 */
	public String getCookie() {
		return cookie;
	}

	/**
	 * @return the userAgentBrowser
	 */
	public String getUserAgentBrowser() {
		return userAgentBrowser;
	}

	/**
	 * @return the idOrderPayU
	 */
	public String getIdOrderPayU() {
		return idOrderPayU;
	}

	/**
	 * @return the idTransactionPayU
	 */
	public String getIdTransactionPayU() {
		return idTransactionPayU;
	}

	/**
	 * @return the stateTransactionPayU
	 */
	public String getStateTransactionPayU() {
		return stateTransactionPayU;
	}

	/**
	 * @param idTransaccion the idTransaccion to set
	 */
	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	
	/**
	 * @param deviceSessionId the deviceSessionId to set
	 */
	public void setDeviceSessionId(String deviceSessionId) {
		this.deviceSessionId = deviceSessionId;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @param cookie the cookie to set
	 */
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	/**
	 * @param userAgentBrowser the userAgentBrowser to set
	 */
	public void setUserAgentBrowser(String userAgentBrowser) {
		this.userAgentBrowser = userAgentBrowser;
	}

	/**
	 * @param idOrderPayU the idOrderPayU to set
	 */
	public void setIdOrderPayU(String idOrderPayU) {
		this.idOrderPayU = idOrderPayU;
	}

	/**
	 * @param idTransactionPayU the idTransactionPayU to set
	 */
	public void setIdTransactionPayU(String idTransactionPayU) {
		this.idTransactionPayU = idTransactionPayU;
	}

	/**
	 * @param stateTransactionPayU the stateTransactionPayU to set
	 */
	public void setStateTransactionPayU(String stateTransactionPayU) {
		this.stateTransactionPayU = stateTransactionPayU;
	}

	/**
	 * @return the idProducto
	 */
	public String getIdProducto() {
		return idProducto;
	}

	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	/**
	 * @return the idCliente
	 */
	public String getIdCliente() {
		return idCliente;
	}

	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	/**
	 * @return the fechaTransaccion
	 */
	public Date getFechaTransaccion() {
		return fechaTransaccion;
	}

	/**
	 * @param fechaTransaccion the fechaTransaccion to set
	 */
	public void setFechaTransaccion(Date fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}
		
}