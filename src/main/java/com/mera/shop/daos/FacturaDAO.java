package com.mera.shop.daos;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.mera.shop.entidades.Factura;

/**
 * Objeto DAO para la manipulacion de datos de facturas
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Repository
@Qualifier("facturasRepositorio")
public class FacturaDAO implements IRepositorio<Factura> {

	/***
	 * Lista de facturas emitidas
	 */
	private LinkedList<Factura> facturas;


	/***
	 * Constructor
	 */
	private FacturaDAO() {
		facturas = new LinkedList<Factura>();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#buscarElementoPorId(java.lang.String)
	 */
	@Override
	public Factura buscarElementoPorId(String id) {
		for (Factura factura : facturas) {
			if (factura.getIdFactura().equals(id)) {
				return factura;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#obtenerTodosElementos()
	 */
	@Override
	public List<Factura> obtenerTodosElementos() {
		return facturas;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#agregarElemento(java.lang.Object)
	 */
	@Override
	public void agregarElemento(Factura newElement) {
		facturas.add(newElement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#actualizarElemento(java.lang.Object)
	 */
	@Override
	public void actualizarElemento(Factura updatedElement) {
		for(int i = 0; i < facturas.size(); i++){
			if(facturas.get(i).getIdFactura().equals(updatedElement.getIdFactura())){
				facturas.set(i, updatedElement);
				break;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#eliminarElemento(java.lang.String)
	 */
	@Override
	public void eliminarElemento(String id) {
		for(int i = 0; i < facturas.size(); i++){
			if(facturas.get(i).getIdFactura().equals(id)){
				facturas.remove(i);
				break;
			}
		}
	}

}
