/**
 * 
 */
package com.mera.shop.daos;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.mera.shop.entidades.Cliente;

/**
 * Objeto DAO para la manipulacion de datos de clientes
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Repository
@Qualifier("clientesRepositorio")
public class ClienteDAO implements IRepositorio<Cliente> {

	/***
	 * Lista de clientes
	 */
	private LinkedList<Cliente> clientes;

	/***
	 * Constructor
	 */
	private ClienteDAO() {
		clientes = new LinkedList<Cliente>();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#buscarElementoPorId(java.lang.String)
	 */
	@Override
	public Cliente buscarElementoPorId(String id) {
		for (Cliente cliente : clientes) {
			if (cliente.getIdCliente().equals(id)) {
				return cliente;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#obtenerTodosElementos()
	 */
	@Override
	public List<Cliente> obtenerTodosElementos() {
		return clientes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#agregarElemento(java.lang.Object)
	 */
	@Override
	public void agregarElemento(Cliente nuevoCliente) {
		clientes.add(nuevoCliente);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#actualizarElemento(java.lang.Object)
	 */
	@Override
	public void actualizarElemento(Cliente clienteActualizado) {
		for (int i = 0; i < clientes.size(); i++) {
			if (clientes.get(i).getIdCliente()
					.equals(clienteActualizado.getIdCliente())) {
				clientes.set(i, clienteActualizado);
				break;
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#eliminarElemento(java.lang.String)
	 */
	@Override
	public void eliminarElemento(String id) {
		for (int i = 0; i < clientes.size(); i++) {
			if (clientes.get(i).getIdCliente().equals(id)) {
				clientes.remove(i);
				break;
			}
		}
	}

}