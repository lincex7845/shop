/**
 * 
 */
package com.mera.shop.daos;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.mera.shop.entidades.Inventario;

/**
 * Objeto DAO para la manipulacion de datos del inventario
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Repository
@Qualifier("inventarioRepositorio")
public class InventarioDAO implements IRepositorio<Inventario> {

	/***
	 * Lista de existencia en inventario
	 */
	private LinkedList<Inventario> inventarios;
	
	/***
	 * Constructor
	 */
	private InventarioDAO(){
		inventarios = new LinkedList<Inventario>();
	}

		
	/** 
	 * Implementacion para buscar el registro de un producto en el inventario
	 * @param idProducto	Identificador del producto
	 * @see com.mera.shop.daos.IRepositorio#buscarElementoPorId(java.lang.String)
	 */
	@Override
	public Inventario buscarElementoPorId(String idProducto) {
		for(Inventario inventario : inventarios){
			if(inventario.getProducto().getIdProducto().equals(idProducto)){
				return inventario;
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.DAO.IRepositorio#obtenerTodosElementos()
	 */
	@Override
	public List<Inventario> obtenerTodosElementos() {
		return inventarios;
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.DAO.IRepositorio#agregarElemento(java.lang.Object)
	 */
	@Override
	public void agregarElemento(Inventario newElement) {
		inventarios.add(newElement);
	}

	/* (non-Javadoc)
	 * @see com.mera.shop.DAO.IRepositorio#actualizarElemento(java.lang.Object)
	 */
	@Override
	public void actualizarElemento(Inventario updatedElement) {
		for(int i = 0; i < inventarios.size(); i++){
			if(inventarios.get(i).getIdInventario().equals(updatedElement.getIdInventario())){
				inventarios.set(i, updatedElement);
				break;
			}
		}
	}

	/**
	 * Implementacion para eliminar un registro del inventario
	 * @param idInventairo	Identificador del producto en el inventario
	 * @see com.mera.shop.daos.IRepositorio#eliminarElemento(java.lang.String)
	 */
	@Override
	public void eliminarElemento(String idInventairo) {
		for(int i = 0; i < inventarios.size(); i++){
			if(inventarios.get(i).getIdInventario().equals(idInventairo)){
				inventarios.remove(i);
				break;
			}
		}
	}
	
}
