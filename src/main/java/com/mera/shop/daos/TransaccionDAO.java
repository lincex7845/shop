/**
 * 
 */
package com.mera.shop.daos;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.mera.shop.entidades.Transaccion;

/**
 * Objeto DAO para la manipulacion de datos de las transacciones asociadas a las
 * ventas
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Repository
@Qualifier("transaccionesRepositorio")
public class TransaccionDAO implements IRepositorio<Transaccion> {

	/***
	 * Lista de transacciones
	 */
	private LinkedList<Transaccion> transacciones;

	/***
	 * Constructor
	 */
	private TransaccionDAO() {
		transacciones = new LinkedList<Transaccion>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#buscarElementoPorId(java.lang.String)
	 */
	@Override
	public Transaccion buscarElementoPorId(String id) {
		for (Transaccion transaccion : transacciones) {
			if (transaccion.getIdTransaccion().equals(id)) {
				return transaccion;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#obtenerTodosElementos()
	 */
	@Override
	public List<Transaccion> obtenerTodosElementos() {
		return transacciones;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#agregarElemento(java.lang.Object)
	 */
	@Override
	public void agregarElemento(Transaccion newElement) {
		transacciones.add(newElement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#actualizarElemento(java.lang.Object)
	 */
	@Override
	public void actualizarElemento(Transaccion updatedElement) {
		for (int i = 0; i < transacciones.size(); i++) {
			if (transacciones.get(i).getIdTransaccion()
					.equals(updatedElement.getIdTransaccion())) {
				transacciones.set(i, updatedElement);
				break;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#eliminarElemento(java.lang.String)
	 */
	@Override
	public void eliminarElemento(String id) {
		for (int i = 0; i < transacciones.size(); i++) {
			if (transacciones.get(i).getIdTransaccion().equals(id)) {
				transacciones.remove(i);
				break;
			}
		}
	}

}
