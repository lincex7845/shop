/**
 * 
 */
package com.mera.shop.daos;

import java.util.List;

import javax.validation.Valid;

/**
 * Interfaz de la cual implementaran los repositorios los metodos para acceso a
 * datos
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public interface IRepositorio<T> {

	/**
	 * Definicion abstracta para obtener un elemento por su identificador
	 * 
	 * @param id
	 *            Identificador del elemento
	 * @return Elemento que contiene el identificador requerido
	 */
	public T buscarElementoPorId(String id);
	

	/***
	 * Definicion abstracta para obtener todos los elementos en una coleccion
	 * 
	 * @return Todos los elementos que son instancia de una entidad de datos
	 */
	public List<T> obtenerTodosElementos();

	/***
	 * Definicion abstracta para agregar una nueva entidad
	 * 
	 * @param nuevoElemento
	 *            Nueva entidad de datos
	 */
	@Valid
	public void agregarElemento(T nuevoElemento);

	/***
	 * Definicion abstracta para actualizar una entidad de datos
	 * 
	 * @param elementoActualizado
	 *            Entidad de datos actualizada
	 */
	public void actualizarElemento(T elementoActualizado) ;

	/***
	 * Definicion abstracta para eliminar una entidad dado su identificador
	 * 
	 * @param id
	 *            Identificador del elemento
	 */
	public void eliminarElemento(String id) ;
	
}
