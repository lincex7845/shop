/**
 * 
 */
package com.mera.shop.daos;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.mera.shop.entidades.Producto;

/**
 * Objeto DAO para la manipulacion de datos de productos
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Repository
@Qualifier("productosRepositorio")
public class ProductoDAO implements IRepositorio<Producto> {

	/***
	 * Lista de productos
	 */
	private LinkedList<Producto> productos;


	/***
	 * Constructor
	 */
	private ProductoDAO() {
		productos = new LinkedList<Producto>();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#buscarElementoPorId(java.lang.String)
	 */
	@Override
	public Producto buscarElementoPorId(String id) {
		for (Producto producto : productos) {
			if (producto.getIdProducto().equals(id)) {
				return producto;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#obtenerTodosElementos()
	 */
	@Override
	public List<Producto> obtenerTodosElementos() {
		return productos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#agregarElemento(java.lang.Object)
	 */
	@Override
	public void agregarElemento(Producto newElement) {
		productos.add(newElement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#actualizarElemento(java.lang.Object)
	 */
	@Override
	public void actualizarElemento(Producto updatedElement) {
		for (int i = 0; i < productos.size(); i++) {
			if (productos.get(i).getIdProducto()
					.equals(updatedElement.getIdProducto())) {
				productos.set(i, updatedElement);
				break;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.mera.shop.DAO.IRepositorio#eliminarElemento(java.lang.String)
	 */
	@Override
	public void eliminarElemento(String id) {
		for (int i = 0; i < productos.size(); i++) {
			if (productos.get(i).getIdProducto().equals(id)) {
				productos.remove(i);
				break;
			}
		}
	}

}
