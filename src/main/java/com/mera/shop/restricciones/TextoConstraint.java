/**
 * 
 */
package com.mera.shop.restricciones;

import java.io.Serializable;
import java.util.regex.Pattern;

import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zul.Constraint;
import org.zkoss.zul.Textbox;

/**
 * @author david.mera
 *
 */
public class TextoConstraint implements Constraint, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7291925162579437304L;
	private int numeroMaximoCaracteres;
	private Pattern patronExpresionRegular;
		

	/**
	 * @param numeroMaximoCaracteres
	 * @param expresionRegular
	 */
	public TextoConstraint(int numeroMaximoCaracteres, String expresionRegular) {
		super();
		this.numeroMaximoCaracteres = numeroMaximoCaracteres;
		this.patronExpresionRegular = Pattern.compile(expresionRegular);
	}

	@Override
	public void validate(Component comp, Object value) throws WrongValueException {
		if(comp instanceof Textbox){
			String enteredValue = (String) value;
			if (enteredValue.isEmpty()) {
				throw new WrongValueException(comp, "Este dato no puede estar vacio");
			}
			if(!patronExpresionRegular.matcher(enteredValue).matches()){
				throw new WrongValueException(comp, "El valor digitado no corresponde al dato solicitado");
			}
			if(enteredValue.length() > this.numeroMaximoCaracteres){
				throw new WrongValueException(comp, "El valor digitado excede el maximo numero de caracteres permitido");
			}
		}
		
	}

}
