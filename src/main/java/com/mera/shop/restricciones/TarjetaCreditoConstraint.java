/**
 * 
 */
package com.mera.shop.restricciones;

import java.io.Serializable;
import java.util.regex.Pattern;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zul.Constraint;
import org.zkoss.zul.Textbox;
import com.mera.shop.utilidades.FranquiciaTarjetaCredito;
import com.mera.shop.utilidades.ValidadorUtil;

/**
 * @author david.mera
 *
 */
public class TarjetaCreditoConstraint implements Constraint, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6902413638644328904L;
	private FranquiciaTarjetaCredito franquicia;
	private static final Pattern PATRONNUMEROS = Pattern.compile("\\d+");
	
	public TarjetaCreditoConstraint(FranquiciaTarjetaCredito franquicia){
		super();
		this.franquicia = franquicia;
	}
	

	/* (non-Javadoc)
	 * @see org.zkoss.zul.Constraint#validate(org.zkoss.zk.ui.Component, java.lang.Object)
	 */
	@Override
	public void validate(Component comp, Object value) throws WrongValueException {
		if(comp instanceof Textbox){
			String enteredValue = (String) value;
			if (enteredValue.isEmpty()) {
				throw new WrongValueException(comp, "El numero de la tarjeta no puede ser vacio");
			}
			if(!PATRONNUMEROS.matcher(enteredValue).matches()){
				throw new WrongValueException(comp, "El valor digitado no corresponde a un numero de tarjeta credito");
			}
			if(!ValidadorUtil.isTarjetaCreditoValida(franquicia, enteredValue)){
				throw new WrongValueException(comp, "El numero digitado no es valido para la franquicia seleccionada");
			}
		}
	}

}
