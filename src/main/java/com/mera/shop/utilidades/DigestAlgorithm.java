/**
 * 
 */
package com.mera.shop.utilidades;

/**
 * Enumeracion que contiene instancias de algoritmos de encriptacion
 * para la validación de tarjetas de credito
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public enum DigestAlgorithm {

	MD5("MD5"),
	SHA_1("SHA-1"),
	SHA_256("SHA-256"),
	SHA_512("SHA-512"),
	HMAC_256("HmacSHA256"),
	HMAC_512("HmacSHA512");
	
	private String valor;
	
	private DigestAlgorithm(String valor){
		this.valor = valor;
	}
	
	public String getValor(){
		return valor;
	}
}
