package com.mera.shop.utilidades;

/**
 * Clase que contiene m�todos para la validaci�n de datos
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public class ValidadorUtil {

	private ValidadorUtil(){
		//constructor
	}
	
	/**
	 * Este m�todo verifica si el texto/cadena es v�lido(a)
	 * 
	 * @param string
	 *            El texto o cadena a verificar
	 * @return Resultado de la verificacion. Verdadero si el texto no esta vaci�
	 *         o es diferente de nulo. En caso contrario, falso.
	 */
	public static boolean isTextoValido(String string) {
		return (string != null) && (!string.isEmpty());
	}

	/***
	 * Realiza verificacion del n�mero de tarjeta de credito implementando el
	 * algoritmo Luhn
	 * 
	 * @param numeroTarjetaCredito
	 *            Numero de la tarjeta de credito
	 * @return Resultado de la verificacion
	 * @see <a
	 *      href="http://www.chriswareham.demon.co.uk/software/Luhn.java">Luhn
	 *      Algorithm by Chris Wareham</a>
	 */
	public static boolean isNumeroTarjetaCreditoValido(
			String numeroTarjetaCredito) {
		int suma = 0;
		boolean alternar = false;
		for (int i = numeroTarjetaCredito.length() - 1; i >= 0; i--) {
			int n = Integer.parseInt(numeroTarjetaCredito.substring(i, i + 1));
			if (alternar) {
				n *= 2;
				if (n > 9) {
					n = (n % 10) + 1;
				}
			}
			suma += n;
			alternar = !alternar;
		}
		return suma % 10 == 0;
	}

	/***
	 * Verifica si el n�mero de una tarjeta de cr�dito es v�lido
	 * para su respectiva franquicia
	 * @param franquicia		Franquicia a la que pertenece la tarjeta
	 * @param numeroTarjetaCredito	N�mero de tarjeta a comprobar
	 * @return	Resultado de la verificaci�n
	 */
	public static boolean isTarjetaCreditoValida(
			FranquiciaTarjetaCredito franquicia, String numeroTarjetaCredito) {
		return numeroTarjetaCredito.length() == franquicia.getDigitosNumeroTarjeta()
				&& isNumeroTarjetaCreditoValido(numeroTarjetaCredito);
	}
}
