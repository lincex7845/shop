/**
 * 
 */
package com.mera.shop.utilidades;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.CharEncoding;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.params.HttpConnectionParams;
import org.jboss.logging.*;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Objeto que encapsulta m�todos para realizar peticiones HTTP
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public class RestUtil {

	private static final Logger LOGGER = Logger.getLogger(RestUtil.class
			.getName());
	
	private static HttpClient httpClient;

	private RestUtil() {
		// constructor
	}

	/***
	 * M�todo para realizar peticiones HTTP. Se recomienda cerrar la instancia de {@link org.apache.http.client.HttpClient} luego de ser obtenido el resultado
	 *
	 * @param metodo
	 *            Metodo HTTP a ejecutar
	 * @param url
	 *            URL en la que se realiza la petici�n
	 * @param parametros
	 *            Parametros a agregar en la URL para la peticion
	 * @param cabeceras
	 *            Cabecerar a agregar en la petici�n
	 * @param timeout
	 *            Tiempo de espera m�ximo para la petici�n. Est� en milisegundos
	 * @return Objeto con la respuesta del procesamiento. Puede ser nulo si
	 *         ocurr�o un error durante el procesamiento
	 */
	public static HttpResponse ejecutarPeticionHTTP(MetodoHTTP metodo,
			@NotEmpty String url, Map<String, String> parametros,
			String jsonString, Map<String, String> cabeceras, int timeout) {
		HttpResponse response = null;
		RestUtil.httpClient = construirHttpClient(timeout);
		try {
			URIBuilder uriBuilder = agregarParametros(new URIBuilder(url),
					parametros);
			HttpUriRequest request = obtenerHttpRequest(metodo, uriBuilder,
					cabeceras, jsonString);
			response = RestUtil.httpClient.execute(request);
			LOGGER.debug("Respuesta " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
		} catch (URISyntaxException e) {
			LOGGER.error("Error al procesar URL de peticion a " + url, e);
		} catch (ClientProtocolException e) {
			LOGGER.error("Error al procesar peticion a " + url, e);
		} catch (IOException e) {
			LOGGER.error("Error al procesar parametros para la peticion a "
					+ url, e);
		} catch (Exception e) {
			LOGGER.error("Error inesperado al procesar peticion a " + url,
					e);
		} 
		return response;
	}
	
	/***
	 * Permite cerrar el cliente Http
	 */
	public static void cerrarClienteHTTP(){
		if(RestUtil.httpClient != null){
			RestUtil.httpClient.getConnectionManager().shutdown();
			RestUtil.httpClient = null;
		}
	}

	/***
	 * Metodo para obtener la peticion a ejecutar
	 * 
	 * @param metodo
	 *            Metodo HTTP a ejecutar
	 * @param uriBuilder
	 *            Constructor de URL de la peticion
	 * @param cabeceras
	 *            Cabeceras a agregar en la peticion
	 * @param jsonString
	 *            Entidad JSON a emitir en la peticion
	 * @return Objeto configurado para realizar peticion HTTP
	 * @throws URISyntaxException
	 *             Si ocurre un error al fijar la URL de la peticion
	 * @throws UnsupportedEncodingException
	 *             Si ocurre un error al establecer el objeto JSON como entidad
	 */
	private static HttpUriRequest obtenerHttpRequest(MetodoHTTP metodo,
			URIBuilder uriBuilder, Map<String, String> cabeceras,
			String jsonString) throws URISyntaxException,
			UnsupportedEncodingException {
		HttpUriRequest request = null;
		StringEntity entidad = null;
		if (ValidadorUtil.isTextoValido(jsonString)) {
			entidad = new StringEntity(jsonString);
		}
		switch (metodo) {
		case POST:
			HttpPost postRequest = (HttpPost) agregarCabeceras(new HttpPost(
					uriBuilder.build()), cabeceras);
			postRequest.setEntity(entidad);
			request = postRequest;
			break;
		case PUT:
			HttpPut putRequest = (HttpPut) agregarCabeceras(new HttpPut(
					uriBuilder.build()), cabeceras);
			putRequest.setEntity(entidad);
			request = putRequest;
			break;
		case DELETE:
			HttpDelete deleteRequest = (HttpDelete) agregarCabeceras(
					new HttpDelete(uriBuilder.build()), cabeceras);
			request = deleteRequest;
			break;
		case GET:
			HttpGet getRequest = (HttpGet) agregarCabeceras(new HttpGet(
					uriBuilder.build()), cabeceras);
			request = getRequest;
			break;
		default:
			break;
		}
		return request;
	}

	/**
	 * Metodo para configurar y construir cliente HTTP
	 * 
	 * @param timeout
	 *            Tiempo de espera m�ximo para la conexion
	 * @return Cliente HTTP configurado con el tiempo de espera deseado
	 */
	private static HttpClient construirHttpClient(int timeout) {
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), timeout);
		HttpConnectionParams.setSoTimeout(client.getParams(), timeout);
		return client;
	}

	/***
	 * Metodo que permite agregar parametros a una URL
	 * 
	 * @param uriBuilder
	 *            Constructor de la URL
	 * @param parametros
	 *            Conjunto de datos mapeados por el respectivo nombre del
	 *            parametro como llave
	 * @return Objeto encargado de construir la URL, junto con los parametros a
	 *         agregar, si es que se requieren.
	 * @throws UnsupportedEncodingException
	 *             Se lanza cuando el valor de un parametro no puede codificarse
	 *             a UTF-8
	 */
	private static URIBuilder agregarParametros(URIBuilder uriBuilder,
			Map<String, String> parametros) throws UnsupportedEncodingException {
		URIBuilder nuevoUriBuilder = uriBuilder;
		if (parametros != null && parametros.size() > 0) {
			for (Entry<String, String> entry : parametros.entrySet()) {
				if (ValidadorUtil.isTextoValido(entry.getValue())) {
					nuevoUriBuilder.addParameter(entry.getKey(), URLEncoder
							.encode(entry.getValue(), CharEncoding.UTF_8));
				}
			}
		}
		return nuevoUriBuilder;
	}

	/***
	 * Metodo para agregar cabeceras a una peticion/mensaje HTTP
	 * 
	 * @param mensaje
	 *            Peticion/mensaje a procesar
	 * @param cabeceras
	 *            Conjunto de cabeceras, mapeadas por su nombre como llave
	 * @return Peticion/mensaje con las cabeceras agregadas, si es que estas se
	 *         requieren
	 */
	private static AbstractHttpMessage agregarCabeceras(
			AbstractHttpMessage mensaje, Map<String, String> cabeceras) {
		AbstractHttpMessage nuevoMensaje = mensaje;
		if (cabeceras != null && cabeceras.size() > 0) {
			for (Entry<String, String> entry : cabeceras.entrySet()) {
				if (ValidadorUtil.isTextoValido(entry.getValue())) {
					nuevoMensaje.addHeader(entry.getKey(), entry.getValue());
				}
			}
		}
		nuevoMensaje.addHeader(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON );
		nuevoMensaje.addHeader(HttpHeaders.ACCEPT ,MediaType.APPLICATION_JSON );
		return mensaje;
	}
	
}