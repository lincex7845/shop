/**
 * 
 */
package com.mera.shop.utilidades;

/**
 * Enumeracion que contiene los tipos de m�todos que se procesan por HTTP
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public enum MetodoHTTP{
	
	/***
	 * Metodo GET
	 */
	GET("GET"),
	/***
	 * Metodo POST
	 */
	POST("POST"),
	/***
	 * Metodo PUT
	 */
	PUT("PUT"),
	/***
	 * Metodo DELETE
	 */
	DELETE("DELETE");
	
	/**
	 * Descripction del metodo HTTP
	 */
	private String metodo;
	/***
	 * constructor
	 * @param metodo
	 */
	private MetodoHTTP(String metodo){
		this.metodo = metodo;
	}
	
	/***
	 * 
	 * @return descripcion del metodo HTTP
	 */
	public String getMetodo(){
		return metodo;
	}	
}