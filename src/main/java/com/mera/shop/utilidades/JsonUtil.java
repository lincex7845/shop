/**
 * 
 */
package com.mera.shop.utilidades;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Objeto que encapsulta funcionalidades para parsear JSON usando Jackson
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public class JsonUtil {

	/***
	 * Metodo para obtener el respectivo JSON string de un objeto
	 * 
	 * @param object
	 *            Objeto a ser serializado
	 * @return JSON string del objeto
	 * @throws JsonProcessingException
	 *             Si ocurre un error al parsear el objeto a JSON
	 */
	public static String obtenerJsonString(Object object) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		return mapper.writeValueAsString(object);
	}

	/***
	 * Metodo para parsear un JSON string a un objeto
	 * 
	 * @param jsonString
	 *            JSON en formato string
	 * @param clase
	 *            Clase a la que debe ser parseado el JSON
	 * @return Instancia de la clase suministrada
	 * @throws JsonParseException
	 *             Si ocurre un error al parsear el JSON string al objeto
	 * @throws JsonMappingException
	 *             Si la representacion del JSON no coincide con la suministrada
	 *             en la clase
	 * @throws IOException
	 *             Si ocurre un error de serialziacion
	 */
	public static <T> T obtenerObjetoDeJsonString(String jsonString, Class<T> clase)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(jsonString, clase);
	}

	/***
	 * Metodo para parsear un JSON contenido en InputStream a un objeto
	 * 
	 * @param jsonString
	 *            Stream de JSON
	 * @param clase
	 *            Clase a la que debe ser parseado el JSON
	 * @return Instancia de la clase suministrada
	 * @throws JsonParseException
	 *             Si ocurre un error al parsear el stream al objeto
	 * @throws JsonMappingException
	 *             Si la representacion del JSON no coincide con la suministrada
	 *             en la clase
	 * @throws IOException
	 *             Si ocurre un error de serialziacion
	 */
	public static <T> T obtenerObjetoDeJsonInputStream(InputStream jsonStream, Class<T> clase)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(jsonStream, clase);
	}
}