/**
 * 
 */
package com.mera.shop.utilidades;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.core.HttpHeaders;
import org.apache.http.HttpResponse;
import org.jboss.logging.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.mera.shop.entidades.Transaccion;
import com.mera.shop.entidades.payu.PeticionConsultaPayU;
import com.mera.shop.entidades.payu.PeticionReembolsoPayU;
import com.mera.shop.entidades.payu.RespuestaConsultaOrdenPayU;
import com.mera.shop.entidades.payu.RespuestaReembolsoPayU;
import com.payu.sdk.model.Merchant;
import com.payu.sdk.model.Order;
import com.payu.sdk.model.Transaction;
import com.payu.sdk.model.TransactionType;
import com.payu.sdk.model.request.Command;

/**
 * Objeto que sirve funcionalidades para consumir API REST PayU
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Component("payuReportsUtil")
public class PayuAPIUtil {

	/***
	 * Bean que contiene las propiedades de la aplicacion
	 */
	private static ShopPropertiesUtil shopProperties;

	/**
	 * Objeto para registrar eventos
	 */
	private static final Logger LOGGER = Logger.getLogger(PayuAPIUtil.class
			.getName());

	/**
	 * Constructor por defecto
	 */
	@Autowired
	public void setShopPropertiesUtil(ShopPropertiesUtil shopProperties) {
		PayuAPIUtil.shopProperties = shopProperties;
	}

	/***
	 * Metodo para consultar estado de una orden en PayU
	 * 
	 * @param idOrdenPayU
	 *            Identificador de la orden en PayU
	 * @return Objeto que representa la respuesta de la API PayU
	 */
	public static RespuestaConsultaOrdenPayU consultarEstadoOrderPayUPorId(
			String idOrdenPayU) {
		RespuestaConsultaOrdenPayU respuesta = null;
		Map<String, Integer> details = new HashMap<String, Integer>();
		details.put("orderId", Integer.valueOf(idOrdenPayU));
		Merchant merchant = new Merchant();
		merchant.setApiKey(shopProperties.getApiKey());
		merchant.setApiLogin(shopProperties.getApiLogin());
		PeticionConsultaPayU peticion = new PeticionConsultaPayU(
				Boolean.valueOf(shopProperties.getIsTest()),
				shopProperties.getLanguage(), Command.ORDER_DETAIL.name(),
				merchant, details);
		Map<String, String> cabeceras = new HashMap<String, String>();
		cabeceras.put(
				HttpHeaders.AUTHORIZATION,
				SeguridadUtil.obtenerCabeceraAutenticacionBasica(
						shopProperties.getApiLogin(),
						shopProperties.getApiKey()).getValue());
		String jsonString = null;
		try {
			jsonString = JsonUtil.obtenerJsonString(peticion);
			LOGGER.debug("Peticion a " + shopProperties.getFullReportsUrl()
					+ " enviando " + jsonString);
			HttpResponse httpResponse = RestUtil.ejecutarPeticionHTTP(
					MetodoHTTP.POST, shopProperties.getFullReportsUrl(), null,
					jsonString, cabeceras,
					Integer.parseInt(shopProperties.getTimeout()));
			if (httpResponse != null) {
				respuesta = JsonUtil.obtenerObjetoDeJsonInputStream(
						httpResponse.getEntity().getContent(),
						RespuestaConsultaOrdenPayU.class);
			}
		} catch (NumberFormatException | IllegalStateException | IOException e) {
			LOGGER.error(e);
		} finally {
			RestUtil.cerrarClienteHTTP();
		}
		return respuesta;
	}

	/***
	 * Metodo para realizar reembolsos a traves de la API
	 * 
	 * @param transaccion
	 *            Objeto con informacion de la orden y transaccion en PayU
	 * @param razon
	 *            Motivo por el que se realiza el reembolso
	 * @return Objeto que contiene respuesta del API PayU. Puede ser nulo si
	 *         ocurre un error en la conexion
	 */
	public static RespuestaReembolsoPayU reembolsarOrderPayU(
			Transaccion transaccion, String razon) {
		RespuestaReembolsoPayU respuesta = null;
		PeticionReembolsoPayU peticion = new PeticionReembolsoPayU();
		peticion.setTest(Boolean.valueOf(shopProperties.getIsTest()));
		peticion.setCommand(Command.SUBMIT_TRANSACTION.name());
		peticion.setLanguage(shopProperties.getLanguage());
		Merchant merchant = new Merchant();
		merchant.setApiKey(shopProperties.getApiKey());
		merchant.setApiLogin(shopProperties.getApiLogin());
		peticion.setMerchant(merchant);
		Order orderPayu = new Order();
		orderPayu.setId(Integer.valueOf(transaccion.getIdOrderPayU()));
		Transaction transactionPayU = new Transaction();
		transactionPayU.setOrder(orderPayu);
		transactionPayU.setParentTransactionId(transaccion
				.getIdTransactionPayU());
		transactionPayU.setType(TransactionType.REFUND);
		transactionPayU.setReason(razon);
		peticion.setTransaction(transactionPayU);
		Map<String, String> cabeceras = new HashMap<String, String>();
		cabeceras.put(
				HttpHeaders.AUTHORIZATION,
				SeguridadUtil.obtenerCabeceraAutenticacionBasica(
						shopProperties.getApiLogin(),
						shopProperties.getApiKey()).getValue());
		String jsonString = null;
		try {
			jsonString = JsonUtil.obtenerJsonString(peticion);
			LOGGER.debug("Peticion a " + shopProperties.getFullPaymentsUrl()
					+ " enviando " + jsonString);
			HttpResponse httpResponse = RestUtil.ejecutarPeticionHTTP(
					MetodoHTTP.POST, shopProperties.getFullPaymentsUrl(), null,
					jsonString, cabeceras,
					Integer.parseInt(shopProperties.getTimeout()));
			if (httpResponse != null) {
				respuesta = JsonUtil.obtenerObjetoDeJsonInputStream(
						httpResponse.getEntity().getContent(),
						RespuestaReembolsoPayU.class);
			}
		} catch (NumberFormatException | IllegalStateException | IOException e) {
			LOGGER.error(e);
		} finally {
			RestUtil.cerrarClienteHTTP();
		}
		return respuesta;
	}
}
