/**
 * 
 */
package com.mera.shop.utilidades;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Enumeration;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.HttpHeaders;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.CharEncoding;
import org.apache.http.Header;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.auth.BasicScheme;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;

/**
 * Objeto que encapsulta m�todos para obtener algunos identificadores
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public class SeguridadUtil {

	/***
	 * Metodo para obtener un identificador pseudo aleatorio �nico a traves de
	 * la clase UUID
	 * 
	 * @return Cadena con identificador �nico
	 */
	public static String estandarUID() {
		return UUID.randomUUID().toString();
	}

	/***
	 * Metodo para obtener un identificador pseudo aleatorio �nico a trav�s de
	 * las clases SecureRandom y BigInteger
	 * 
	 * @return Cadena con identificador �nico
	 */
	public static String secureUID() {
		return new BigInteger(60, new SecureRandom()).toString();
	}

	/***
	 * Metodo para obtener direccion IP del Host
	 * 
	 * @return Direccion IP
	 * @throws UnknownHostException
	 *             Si no es posible resolver la IP del Host
	 */
	public static String obtenerIPAddres() throws UnknownHostException {
		return InetAddress.getLocalHost().getHostAddress();
	}

	/***
	 * Metodo para obtener direccion IP en ZK Framework
	 * 
	 * @return Direccion IP del cliente ZK
	 */
	public static String obtenerIPAddresZKRequest() {
		return Executions.getCurrent().getRemoteAddr();
	}

	/***
	 * Metodo para obtener direccion fisica (MAC) del host
	 * 
	 * @return Direccion MAC
	 * @throws SocketException
	 *             Si ocurre un error al leer las interfaces de red
	 */
	public static String obtenerMACAddress() throws SocketException {
		Enumeration<NetworkInterface> nwInterface;
		nwInterface = NetworkInterface.getNetworkInterfaces();
		while (nwInterface.hasMoreElements()) {
			NetworkInterface nis = nwInterface.nextElement();
			if (nis != null) {
				byte[] mac = nis.getHardwareAddress();
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < mac.length; i++) {
					sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
				}
				return sb.toString();
			}
		}
		return null;
	}

	/***
	 * Permite obtener la cabecera 'User-Agent' de una peticcion HTTP
	 * 
	 * @param request
	 *            Representa la peticion HTTP
	 * @return Valor de la cabecera 'User-Agent' para la peticion
	 */
	public static String obtenerUserAgentBrowser(HttpServletRequest request) {
		return request.getHeader(HttpHeaders.USER_AGENT);
	}

	public static HttpSession obtenerSesionZKRequest() {
		return (HttpSession) Sessions.getCurrent().getNativeSession();
	}

	/***
	 * Permite obtener la cabecera 'User-Agent' de una peticcion HTTP en ZK
	 * 
	 * @param request
	 *            Representa la peticion HTTP
	 * @return Valor de la cabecera 'User-Agent' para la peticion
	 */
	public static String obtenerUserAgentBrowserZKRequest() {
		return Executions.getCurrent().getHeader(HttpHeaders.USER_AGENT);
	}

	/**
	 * Compute the HMAC signature for the given data and shared secret
	 *
	 * @param algorithm
	 *            The algorithm to use (e.g. "HmacSHA512")
	 * @param data
	 *            The data to sign
	 * @param sharedSecret
	 *            The shared secret key to use for signing
	 *
	 * @return A base 64 encoded signature encoded as UTF-8
	 * @throws UnsupportedOperationException
	 *             Si no se puede implementar el algoritmo especificado
	 * @throws IllegalArgumentException
	 *             Si la llave no puede usarse para generar el HMAC
	 */
	public static String computeHMACSignature(String algorithm, byte[] data, byte[] sharedSecret) {
		SecretKey secretKey = new SecretKeySpec(Base64.decodeBase64(sharedSecret), algorithm);
		Mac mac;
		try {
			mac = Mac.getInstance(algorithm);
		} catch (NoSuchAlgorithmException e) {
			throw new UnsupportedOperationException(e);
		}
		try {
			mac.init(secretKey);
		} catch (InvalidKeyException e) {
			throw new IllegalArgumentException(e);
		}
		mac.update(data);
		return Base64.encodeBase64String(mac.doFinal());
	}

	/***
	 * Metodo para obtener un hash/signature hexadecimal de un texto/cadena
	 * 
	 * @param algoritmo
	 *            Instancia del algoritmo a usar para el hash
	 * @param texto
	 *            del hash hexadecimal
	 * @return Hash/signature del texto suministrado
	 * @throws NoSuchAlgorithmException
	 *             Si el algoritmo especificado no est� soportado
	 * @throws UnsupportedEncodingException
	 *             Si el texto no puede ser decodificado a UTF-8
	 */
	public static String computHexHashSignatureFromText(DigestAlgorithm algoritmo, String texto)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		byte[] digest = computeHashSignatureByDigestAlgorithm(algoritmo, texto.getBytes(CharEncoding.UTF_8));
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < digest.length; i++) {
			sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}

	/***
	 * Metodo para obtener un hash/signature en base 64 de un texto/cadena
	 * 
	 * @param algoritmo
	 *            Instancia del algoritmo a usar para el hash
	 * @param texto
	 *            en base 64 del hash
	 * @return Hash/signature del texto suministrado
	 * @throws NoSuchAlgorithmException
	 *             Si el algoritmo especificado no est� soportado
	 * @throws UnsupportedEncodingException
	 *             Si el texto no puede ser decodificado a UTF-8
	 */
	public static String computeBase64HashSignatureFromText(DigestAlgorithm algoritmo, String texto)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		byte[] digest = computeHashSignatureByDigestAlgorithm(algoritmo, texto.getBytes(CharEncoding.UTF_8));
		// retorno =
		return org.apache.commons.codec.binary.Base64.encodeBase64String(digest);
	}

	/***
	 * Metodo para obtener hash de una serie de bytes suministrados
	 * 
	 * @param algoritmo
	 *            Instancia del algoritmo para generar el hash
	 * @param datos
	 *            bytes a encriptar
	 * @return bytes del hash generado
	 * @throws NoSuchAlgorithmException
	 *             Si el algoritmo especificado no est� soportado
	 */
	public static byte[] computeHashSignatureByDigestAlgorithm(DigestAlgorithm algoritmo, byte[] datos)
			throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(algoritmo.getValor());
		md.update(datos);
		return md.digest();
	}

	/***
	 * Metodo para obtener la cabecera de autenticacion basica
	 * 
	 * @param usuario
	 *            nombre de usuario
	 * @param contrasena
	 *            contrase�a
	 * @return Cabecera 'Authorization' con el valor respectivo para las
	 *         credenciales suministradas
	 */
	public static Header obtenerCabeceraAutenticacionBasica(String usuario, String contrasena) {
		Credentials credentials = new UsernamePasswordCredentials(usuario, contrasena);
		return BasicScheme.authenticate(credentials, "UTF-8", false);
	}
}
