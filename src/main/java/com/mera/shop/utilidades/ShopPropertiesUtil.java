/**
 * 
 */
package com.mera.shop.utilidades;

/**
 * Representa las propiedades de la aplicacion
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public class ShopPropertiesUtil {

	private String apiKey;
	private String apiLogin;
	private String publicKey;
	private String merchantId;
	private String accountId;
	private String defaultCookie;
	private String language;
	private String isTest;
	private String timeout;
	private String dateFormat;
	private String paymentsUrl;
	private String reportsUrl;
	private String fullReportsUrl;
	private String fullPaymentsUrl;
	
	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}
	/**
	 * @return the apiLogin
	 */
	public String getApiLogin() {
		return apiLogin;
	}
	/**
	 * @return the publicKey
	 */
	public String getPublicKey() {
		return publicKey;
	}
	/**
	 * @return the merchantId
	 */
	public String getMerchantId() {
		return merchantId;
	}
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * @return the defaultCookie
	 */
	public String getDefaultCookie() {
		return defaultCookie;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @return the isTest
	 */
	public String getIsTest() {
		return isTest;
	}
	/**
	 * @return the timeout
	 */
	public String getTimeout() {
		return timeout;
	}
	/**
	 * @return the dateFormat
	 */
	public String getDateFormat() {
		return dateFormat;
	}
	/**
	 * @return the paymentsUrl
	 */
	public String getPaymentsUrl() {
		return paymentsUrl;
	}
	/**
	 * @return the reportsUrl
	 */
	public String getReportsUrl() {
		return reportsUrl;
	}
	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	/**
	 * @param apiLogin the apiLogin to set
	 */
	public void setApiLogin(String apiLogin) {
		this.apiLogin = apiLogin;
	}
	/**
	 * @param publicKey the publicKey to set
	 */
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	/**
	 * @param merchantId the merchantId to set
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/**
	 * @param defaultCookie the defaultCookie to set
	 */
	public void setDefaultCookie(String defaultCookie) {
		this.defaultCookie = defaultCookie;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * @param isTest the isTest to set
	 */
	public void setIsTest(String isTest) {
		this.isTest = isTest;
	}
	/**
	 * @param timeout the timeout to set
	 */
	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}
	/**
	 * @param dateFormat the dateFormat to set
	 */
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	/**
	 * @param paymentsUrl the paymentsUrl to set
	 */
	public void setPaymentsUrl(String paymentsUrl) {
		this.paymentsUrl = paymentsUrl;
	}
	/**
	 * @param reportsUrl the reportsUrl to set
	 */
	public void setReportsUrl(String reportsUrl) {
		this.reportsUrl = reportsUrl;
	}
	/**
	 * @return the fullReportsUrl
	 */
	public String getFullReportsUrl() {
		return fullReportsUrl;
	}
	/**
	 * @param fullReportsUrl the fullReportsUrl to set
	 */
	public void setFullReportsUrl(String fullReportsUrl) {
		this.fullReportsUrl = fullReportsUrl;
	}
	/**
	 * @return the fullPaymentsUrl
	 */
	public String getFullPaymentsUrl() {
		return fullPaymentsUrl;
	}
	/**
	 * @param fullPaymentsUrl the fullPaymentsUrl to set
	 */
	public void setFullPaymentsUrl(String fullPaymentsUrl) {
		this.fullPaymentsUrl = fullPaymentsUrl;
	}
}
