/**
 * 
 */
package com.mera.shop.utilidades;

/**
 * Enumeracion que contiene la cantidad de digitos por franquicia
 * para la validaci�n de tarjetas de credito
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public enum FranquiciaTarjetaCredito {

	/**
	 * Representa la franquicia Visa y su n�mero de digitos por tarjeta
	 */
	VISA (16,3,"VISA"),
	/**
	 * Representa la franquicia Mastercard y su n�mero de digitos por tarjeta
	 */
	MASTERCARD (16,3,"MASTERCARD"),
	/**
	 * Representa la franquicia American Express y su n�mero de digitos por tarjeta
	 */
	AMEX (15,4,"AMEX"),
	/**
	 * Representa la franquicia Dinner's Club y su n�mero de digitos por tarjeta
	 */
	DINNERS (14,3,"DINNERS");
	
	/**
	 * Cantidad de digitos por franquicia para cada tarjeta
	 */
	private int digitosNumeroTarjeta;
	
	/***
	 * Cantidad de digitos por franquicia para el codigo de seguridad
	 */
	private int digitosCodigoSeguridad;
	
	/**
	 * Descripcion de la franquicia
	 */
	private String nombreFranquicia;

	/**
	 * Constructor
	 * @param digitosNumeroTarjeta	Cantidad digitos predefinidos por la franquicia para sus tarjetas de credito
	 * @param digitosCodigoSeguridad Cantidada digitos predefinidos por la franquicia para verificacion de sus tarjetas
	 * @param nombreFranquicia	Nombre correspondiente a la franquicia
	 * @param
	 */
    private FranquiciaTarjetaCredito(int digitosNumeroTarjeta, int digitosCodigoSeguridad, String nombreFranquicia) {
            this.digitosNumeroTarjeta = digitosNumeroTarjeta;
            this.digitosCodigoSeguridad = digitosCodigoSeguridad;
            this.nombreFranquicia = nombreFranquicia;
    }
    
    /***
     * Devuelve la cantidad de digitos por franquicia para cada una de sus tarjetas
     * @return cantidad de digitos por franquicia
     */
    public int getDigitosNumeroTarjeta(){
    	return digitosNumeroTarjeta;
    }
    
    /***
     * 
     * @return cantidad de digitos de seguridad segun la franquicia
     */
    public int getDigitosCodigoSeguridad(){
    	return digitosCodigoSeguridad;
    }
    
    /**
     * 
     * @return Descripcion de la franquicia
     */
    public String getNombreFranquicia(){
    	return nombreFranquicia;
    }
}
