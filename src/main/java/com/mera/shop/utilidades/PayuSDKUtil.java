/**
 * 
 */
package com.mera.shop.utilidades;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.jboss.logging.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mera.shop.entidades.Cliente;
import com.mera.shop.entidades.Producto;
import com.mera.shop.entidades.PagoTarjetaCredito;
import com.mera.shop.entidades.Transaccion;
import com.payu.sdk.PayU;
import com.payu.sdk.PayUPayments;
import com.payu.sdk.PayUReports;
import com.payu.sdk.exceptions.ConnectionException;
import com.payu.sdk.exceptions.InvalidParametersException;
import com.payu.sdk.exceptions.PayUException;
import com.payu.sdk.model.Currency;
import com.payu.sdk.model.Language;
import com.payu.sdk.model.Order;
import com.payu.sdk.model.PaymentCountry;
import com.payu.sdk.model.TransactionResponse;
import com.payu.sdk.utils.LoggerUtil;

/**
 * Objeto que sirve funcionalidades para consumir SDK PayU
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
@Component("payuSDKUtil")
public class PayuSDKUtil {

	/***
	 * Bean que contiene las propiedades de la aplicacion
	 */
	private static ShopPropertiesUtil shopProperties;
	
	/**
	 * Objeto para registrar eventos
	 */
	private static final Logger LOGGER = Logger.getLogger(PayuSDKUtil.class.getName());

	/**
	 * Constructor por defecto
	 */
	@Autowired
	public PayuSDKUtil(ShopPropertiesUtil shopProperties) {
		PayuSDKUtil.shopProperties = shopProperties;
	}

	/***
	 * Metodo para realizar un pago a traves del SDK de PayU
	 * 
	 * @param transaccion
	 *            Entidad que representa el intento de pago con PayU
	 * @param producto
	 *            Entidad que representa el producto a comprar por parte del
	 *            cliente
	 * @param cliente
	 *            Entidad que representa la persona que realiza el pago
	 * @param pago
	 *            Entidad que representa la informacion asociada al pago
	 * @return Objeto con la respuesta de la transaccion en PayU
	 * @throws IllegalStateException
	 *             Encapsula excepciones
	 *             {@link com.payu.sdk.exceptions.PayUException} y
	 *             {@link com.payu.sdk.exceptions.ConnectionException}
	 * @throws IllegalArgumentException
	 *             Encapsula excepcion
	 *             {@link com.payu.sdk.exceptions.InvalidParametersException}
	 */
	public static TransactionResponse ejecutarPago(Transaccion transaccion, Producto producto, Cliente cliente,
			PagoTarjetaCredito pago) {

		Map<String, String> parameters = new HashMap<String, String>();
		// Configura credenciales
		configurarPayU(shopProperties);
		parameters = configurarCredenciales(shopProperties, parameters);
		// Configura producto a pagar
		parameters = configurarProducto(transaccion, producto, parameters);
		// Configura comprador y pagador--
		parameters = configurarCliente(cliente, parameters);
		// Configura medio de pago
		parameters = configurarPagoConTarjetaCredito(pago, parameters);
		// Configura sesion navegador
		parameters = configurarSesion(transaccion, parameters);
		// Solicitud de autorizaci�n y captura
		TransactionResponse response = null;
		try {
			response = PayUPayments.doAuthorizationAndCapture(parameters);
		} catch (PayUException e) {
			LOGGER.error(e);
			LoggerUtil.error("Error en el procesamiento transaccion PayU", e);
			throw new IllegalStateException(e);
		} catch (InvalidParametersException e) {
			LOGGER.error(e);
			LoggerUtil.error("Error en el envio peticion a PayU", e);
			throw new IllegalArgumentException(e);
		} catch (ConnectionException e) {
			LOGGER.error(e);
			LoggerUtil.error("Error conexion a PayU", e);
			throw new IllegalStateException(e);
		}
		return response;
	}

	/***
	 * Metodo que consulta estado de una orden y sus transacciones en PayU via
	 * SDK
	 * 
	 * @param transaccion
	 *            Representa el intento de pago con PayU
	 * @return Objeto que representa la orden y sus transacciones en PayU
	 * @throws IllegalStateException
	 *             Encapsula excepciones
	 *             {@link com.payu.sdk.exceptions.PayUException} y
	 *             {@link com.payu.sdk.exceptions.ConnectionException}
	 * @throws IllegalArgumentException
	 *             Encapsula excepcion
	 *             {@link com.payu.sdk.exceptions.InvalidParametersException}
	 */
	public static Order ejecutarConsulta(Transaccion transaccion) {
		Map<String, String> parameters = new HashMap<String, String>();
		// Configura credenciales
		configurarPayU(shopProperties);
		parameters.put(PayU.PARAMETERS.ORDER_ID, transaccion.getIdOrderPayU());
		Order response = null;
		try {
			response = PayUReports.getOrderDetail(parameters);
		} catch (PayUException e) {
			LOGGER.error(e);
			LoggerUtil.error("Error en el procesamiento transaccion PayU", e);
			throw new IllegalStateException(e);
		} catch (InvalidParametersException e) {
			LOGGER.error(e);
			LoggerUtil.error("Error en el envio peticion a PayU", e);
			throw new IllegalArgumentException(e);
		} catch (ConnectionException e) {
			LOGGER.error(e);
			LoggerUtil.error("Error conexion a PayU", e);
			throw new IllegalStateException(e);
		}
		return response;
	}

	/***
	 * Metodo para solicitar un reembolso de una transaccion a traves del SDK de
	 * PayU
	 * 
	 * @param transaccion
	 *            Entidad que representa el intento de pago con PayU
	 * @param razonParaReembolso
	 *            Descripcion del motivo para hacer el reembolso
	 *
	 * @return Objeto con la respuesta de la transaccion en PayU
	 */
	public static TransactionResponse ejecutarReembolso(Transaccion transaccion, String razonParaReembolso) {
		Map<String, String> parameters = new HashMap<String, String>();
		// Configura credenciales
		configurarPayU(shopProperties);
		parameters.put(PayU.PARAMETERS.ORDER_ID, transaccion.getIdOrderPayU());
		parameters.put(PayU.PARAMETERS.TRANSACTION_ID, transaccion.getIdTransactionPayU());
		parameters.put(PayU.PARAMETERS.REASON, razonParaReembolso);
		TransactionResponse response = null;
		try {
			response = PayUPayments.doRefund(parameters);
		} catch (PayUException e) {
			LOGGER.error(e);
		} catch (InvalidParametersException e) {
			LOGGER.error(e);
		} catch (ConnectionException e) {
			LOGGER.error(e);
		}
		return response;
	}

	/***
	 * Metodo para probar disponibilidad del api de PayU
	 * 
	 * @return resultado de la validacion. Verdadero si el API REST esta
	 *         disponible, de lo contrario, falso.
	 */
	public static boolean ejecutarPing() {
		boolean response = false;
		try {
			response = PayUPayments.doPing();
		} catch (PayUException e) {
			LOGGER.error(e);
		} catch (ConnectionException e) {
			LOGGER.error(e);
		}
		LOGGER.info("Ping " + response);
		return response;
	}

	private static void configurarPayU(ShopPropertiesUtil shopProperties) {
		PayU.apiKey = shopProperties.getApiKey();
		PayU.apiLogin = shopProperties.getApiLogin();
		PayU.language = Language.valueOf(shopProperties.getLanguage());
		PayU.isTest = Boolean.valueOf(shopProperties.getIsTest());
		LoggerUtil.setLogLevel(Level.ALL); // Incluirlo �nicamente si desea ver
											// toda la traza del log; si solo se
											// desea ver la respuesta, se puede
											// eliminar.
		PayU.paymentsUrl = shopProperties.getPaymentsUrl();
		PayU.reportsUrl = shopProperties.getReportsUrl();
	}

	/***
	 * Metodo para configurar las credenciales de autenticacion del cliente
	 * 
	 * @param shopProperties
	 *            Propiedades de la aplicacion
	 * @param parametros
	 *            Objeto con los parametros a enviar en la peticion
	 * @return Conjunto de parametros mapeados, que incluye las credenciales de
	 *         autenticacion
	 */
	private static Map<String, String> configurarCredenciales(ShopPropertiesUtil shopProperties,
			Map<String, String> parametros) {
		Map<String, String> parametrosConfiguracion = parametros;
		parametrosConfiguracion.put(PayU.PARAMETERS.API_KEY, shopProperties.getApiKey());
		parametrosConfiguracion.put(PayU.PARAMETERS.API_LOGIN, shopProperties.getApiLogin());
		parametrosConfiguracion.put(PayU.PARAMETERS.IS_TEST, shopProperties.getIsTest());
		// Ingrese aqu� el identificador de la cuenta.
		parametrosConfiguracion.put(PayU.PARAMETERS.ACCOUNT_ID, shopProperties.getAccountId());
		// Ingrese aqu� el idioma de la orden.
		parametrosConfiguracion.put(PayU.PARAMETERS.LANGUAGE, Language.es.name()); // "Language.es"
		return parametrosConfiguracion;
	}

	/***
	 * Metodo para configurar el producto a pagar en la peticion
	 * 
	 * @param transaccion
	 *            Entidad que identifica el intento de pago con PayU
	 * @param producto
	 *            Entidad que representa el objeto adquirido por el cliente
	 * @param parametros
	 *            Objeto con los parametros a enviar en la peticion
	 * @return Conjunto de parametros mapeados, que incluye los datos del
	 *         producto a pagar
	 */
	private static Map<String, String> configurarProducto(Transaccion transaccion, Producto producto,
			Map<String, String> parametros) {
		Map<String, String> parametrosConfiguracion = parametros;
		// Ingrese aqu� el c�digo de referencia.
		parametrosConfiguracion.put(PayU.PARAMETERS.REFERENCE_CODE, "" + transaccion.getIdTransaccion());
		// Ingrese aqu� la descripci�n.
		parametrosConfiguracion.put(PayU.PARAMETERS.DESCRIPTION,
				"Pago " + transaccion.getIdProducto());
		// -- Valores --
		// Ingrese aqu� el valor.
		BigDecimal valor = producto.getValorProducto().setScale(3, BigDecimal.ROUND_UP);
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(3);
		df.setMinimumFractionDigits(0);
		df.setGroupingUsed(false);
		parametrosConfiguracion.put(PayU.PARAMETERS.VALUE, "" + df.format(valor));
		// Ingrese aqu� la moneda.
		parametrosConfiguracion.put(PayU.PARAMETERS.CURRENCY, "" + Currency.COP.name());
		return parametrosConfiguracion;
	}

	/***
	 * Metodo para configurar el comprador y pagador en la peticion A partir de
	 * un cliente. Se toman el cliente como comprador y pagador
	 * 
	 * @param cliente
	 *            Entidad que representa la persona que realiza el pago
	 * @param parametros
	 *            Objeto con los parametros a enviar en la peticion
	 * @return Conjunto de parametros mapeados, que incluye los datos de la
	 *         persona que paga
	 */
	private static Map<String, String> configurarCliente(Cliente cliente, Map<String, String> parametros) {
		Map<String, String> parametrosConfiguracion = parametros;
		// Ingrese aqu� el id del comprador.
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_ID, cliente.getIdCliente());
		// Ingrese aqu� el nombre del comprador.
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_NAME, cliente.getNombreCliente());
		// Ingrese aqu� el email del comprador.
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_EMAIL, cliente.getCorreoCliente());
		// Ingrese aqu� el tel�fono de contacto del comprador.
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_CONTACT_PHONE, cliente.getTelefonoCliente());
		// Ingrese aqu� el documento de contacto del comprador.
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_DNI, cliente.getIdCliente());
		// Ingrese aqu� la direcci�n del comprador.
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_STREET, cliente.getResidencia().getDireccionCliente());
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_STREET_2, "");
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_CITY, cliente.getResidencia().getCiudadCliente());
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_STATE, cliente.getResidencia().getEstadoCliente());
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_COUNTRY, cliente.getResidencia().getPaisCliente());
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_POSTAL_CODE,
				cliente.getResidencia().getCodigoPostalCliente());
		parametrosConfiguracion.put(PayU.PARAMETERS.BUYER_PHONE, cliente.getTelefonoCliente());

		// -- Pagador --
		// Ingrese aqu� el id del pagador.
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_ID, cliente.getIdCliente());
		// Ingrese aqu� el nombre del pagador.
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_NAME, cliente.getNombreCliente());
		// Ingrese aqu� el email del pagador.
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_EMAIL, cliente.getCorreoCliente());
		// Ingrese aqu� el tel�fono de contacto del pagador.
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_CONTACT_PHONE, cliente.getTelefonoCliente());
		// Ingrese aqu� el documento de contacto del pagador.
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_DNI, cliente.getIdCliente());
		// Ingrese aqu� la direcci�n del pagador.
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_STREET, cliente.getResidencia().getDireccionCliente());
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_STREET_2, "");
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_CITY, cliente.getResidencia().getCiudadCliente());
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_STATE, cliente.getResidencia().getEstadoCliente());
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_COUNTRY, cliente.getResidencia().getPaisCliente());
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_POSTAL_CODE,
				cliente.getResidencia().getCodigoPostalCliente());
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYER_PHONE, cliente.getTelefonoCliente());
		return parametrosConfiguracion;
	}

	/***
	 * Metodo para configurar el metodo de pago seleccionado por el cliente en
	 * la peticion
	 * 
	 * @param pago
	 *            Entidad que representa los datos de la tarjeta de credito con
	 *            la que se realiza el pago
	 * @param parametros
	 *            Objeto con los parametros a enviar en la peticion
	 * @return Conjunto de parametros mapeados, que incluye los datos de la
	 *         tarjeta a pagar
	 */
	private static Map<String, String> configurarPagoConTarjetaCredito(PagoTarjetaCredito pago,
			Map<String, String> parametros) {
		Map<String, String> parametrosConfiguracion = parametros;
		// -- Datos de la tarjeta de cr�dito --
		// Ingrese aqu� el n�mero de la tarjeta de cr�dito
		parametrosConfiguracion.put(PayU.PARAMETERS.CREDIT_CARD_NUMBER, pago.getNumero());
		// Ingrese aqu� la fecha de vencimiento de la tarjeta de cr�dito
		parametrosConfiguracion.put(PayU.PARAMETERS.CREDIT_CARD_EXPIRATION_DATE,
				pago.getMesExpiracion() > 9 ? String.format("%d/%d", pago.getAnioExpiracion(), pago.getMesExpiracion())
						: String.format("%d/0%d", pago.getAnioExpiracion(), pago.getMesExpiracion()));
		// Ingrese aqu� el c�digo de seguridad de la tarjeta de cr�dito
		parametrosConfiguracion.put(PayU.PARAMETERS.CREDIT_CARD_SECURITY_CODE,
				Integer.toString(pago.getCodigoSeguridad()));
		// Ingrese aqu� el nombre de la tarjeta de cr�dito
		// PaymentMethod.VISA.name())
		parametrosConfiguracion.put(PayU.PARAMETERS.PAYMENT_METHOD, pago.getFranquicia().getNombreFranquicia());
		// Ingrese aqu� el n�mero de cuotas.
		parametrosConfiguracion.put(PayU.PARAMETERS.INSTALLMENTS_NUMBER, Integer.toString(pago.getNumeroCoutas()));
		// Ingrese aqu� el nombre del pais.
		parametrosConfiguracion.put(PayU.PARAMETERS.COUNTRY, PaymentCountry.CO.name());
		return parametrosConfiguracion;
	}

	/***
	 * Metodo para configurar la sesion del cliente en el navegador
	 * 
	 * @param transaccion
	 *            Entidad que identifica el intento de pago con PayU
	 * @param parametros
	 *            Objeto con los parametros a enviar en la peticion
	 * @return Conjunto de parametros mapeados, que incluye los datos de la
	 *         sesion del cliente
	 */
	private static Map<String, String> configurarSesion(Transaccion transaccion, Map<String, String> parametros) {
		Map<String, String> parametrosConfiguracion = parametros;
		// Session id del device. "vghs6tvkcle931686k1900o6e1"
		parametrosConfiguracion.put(PayU.PARAMETERS.DEVICE_SESSION_ID, transaccion.getDeviceSessionId());
		// IP del pagadador
		parametrosConfiguracion.put(PayU.PARAMETERS.IP_ADDRESS, transaccion.getIpAddress());
		// Cookie de la sesi�n actual. "pt1t38347bs6jc9ruv2ecpv7o2"
		parametrosConfiguracion.put(PayU.PARAMETERS.COOKIE, transaccion.getCookie());
		// Cookie de la sesi�n actual.
		// "Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
		parametrosConfiguracion.put(PayU.PARAMETERS.USER_AGENT, transaccion.getUserAgentBrowser());
		return parametrosConfiguracion;
	}

}