package com.mera.shop.payu.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mera.shop.entidades.Cliente;
import com.mera.shop.entidades.Factura;
import com.mera.shop.entidades.Inventario;
import com.mera.shop.entidades.PagoTarjetaCredito;
import com.mera.shop.entidades.Producto;
import com.mera.shop.entidades.Residencia;
import com.mera.shop.entidades.Transaccion;
import com.mera.shop.entidades.payu.RespuestaConsultaOrdenPayU;
import com.mera.shop.entidades.payu.RespuestaReembolsoPayU;
import com.mera.shop.servicios.ClienteServicio;
import com.mera.shop.servicios.FacturaServicio;
import com.mera.shop.servicios.InventarioServicio;
import com.mera.shop.servicios.ProductoServicio;
import com.mera.shop.servicios.TransaccionServicio;
import com.mera.shop.utilidades.DigestAlgorithm;
import com.mera.shop.utilidades.FranquiciaTarjetaCredito;
import com.mera.shop.utilidades.PayuAPIUtil;
import com.mera.shop.utilidades.PayuSDKUtil;
import com.mera.shop.utilidades.SeguridadUtil;
import com.mera.shop.utilidades.ShopPropertiesUtil;
import com.payu.sdk.model.Order;
import com.payu.sdk.model.OrderStatus;
import com.payu.sdk.model.PaymentCountry;
import com.payu.sdk.model.Transaction;
import com.payu.sdk.model.TransactionResponse;
import com.payu.sdk.model.TransactionState;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:com/mera/shop/payu/test/testApplicationContext.xml")
public class ServicioPayUTest {

	@Autowired
	public ShopPropertiesUtil shopProperties;
	@Autowired
	public ClienteServicio clientesServicio;
	@Autowired
	public ProductoServicio productosServicio;
	@Autowired
	public FacturaServicio facturasServicio;
	@Autowired
	public InventarioServicio inventarioServicio;
	@Autowired
	public TransaccionServicio transaccionesServicio1;

	private static Cliente cliente;
	private static Producto producto;
	private static Factura factura;
	private static Transaccion transaccion;
	private static PagoTarjetaCredito pago;

	protected String[] getConfigLocations() {
		return new String[] { "classpath:com/mera/shop/payu/test/testApplicationContext.xml" };
	}

	@Before
	public void setUp() throws NoSuchAlgorithmException, UnknownHostException,
			UnsupportedEncodingException {
		// Crea cliente
		Residencia residencia = new Residencia("Carrera 7 # 26-20", "110010",
				"Bogota", "Bogota DC", PaymentCountry.CO.name());
		cliente = new Cliente("1014741165", "Elizabeth Me   sa Payer",
				"kusanami@gmail.com", "7222620", residencia);
		clientesServicio.agregarNuevoElemento(cliente);
		// Crea producto y registra en inventario
		producto = new Producto("1", "rutaImagen", "producto1", new BigDecimal(
				10));
		productosServicio.agregarNuevoElemento(producto);
		inventarioServicio.agregarNuevoElemento(new Inventario(SeguridadUtil
				.estandarUID(), producto, 11));
		// Crea factura
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
		Date fecha = new Date();
		factura = new Factura(SeguridadUtil.estandarUID(), fecha,
				cliente.getIdCliente(), producto.getIdProducto(), "Compra_"
						+ sf.format(fecha) + "_" + producto.getIdProducto());
		facturasServicio.agregarNuevoElemento(factura);
		// Crea transaccion
		MockHttpServletRequest requestMock = new MockHttpServletRequest();
		MockHttpSession sessionMock = new MockHttpSession();
		requestMock
				.addHeader("User-Agent",
						"Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0");
		requestMock.setSession(sessionMock);
		String deviceSessionId = SeguridadUtil.computHexHashSignatureFromText(
				DigestAlgorithm.MD5,
				sessionMock.getId() + sessionMock.getCreationTime())
				+ cliente.getIdCliente();
		transaccion = new Transaccion(SeguridadUtil.secureUID(),
				producto.getIdProducto(), cliente.getIdCliente(),
				deviceSessionId, SeguridadUtil.obtenerIPAddres(),
				sessionMock.getId(),
				SeguridadUtil.obtenerUserAgentBrowser(requestMock));
		transaccionesServicio1.agregarNuevoElemento(transaccion);
		// Crea pago - tarjeta generada en internet
		// 4485916900225312, CVV2, 498, 04/2018, Visa
		pago = new PagoTarjetaCredito("5303715971862863", 2018, 12, 453,
				FranquiciaTarjetaCredito.MASTERCARD, 1);
		// pago = new PagoTarjetaCredito("4485916900225312", 2018, 04, 498,
		// FranquiciaTarjetaCredito.VISA, 1);
	}

	@Test
	public void testEjecutarPago() {
		TransactionResponse response = PayuSDKUtil.ejecutarPago(transaccion,
				producto, cliente, pago);
		assertNotNull(response);
		assertNotNull(response.getOrderId());
		assertNotNull(response.getTransactionId());
		assertTrue(TransactionState.DECLINED != response.getState()
				&& response.getState() != TransactionState.ERROR);
	}

	@Test
	public void testEjecutarConsulta() {
		transaccion.setIdOrderPayU("7670892");
		Order response = PayuSDKUtil.ejecutarConsulta(transaccion);
		assertNotNull(response);
		assertEquals(response.getId(),
				Integer.valueOf(transaccion.getIdOrderPayU()));
		assertNotNull(response.getBuyer());
		assertEquals(response.getBuyer().getMerchantBuyerId(),
				cliente.getIdCliente());
		assertEquals(response.getBuyer().getFullName(),
				cliente.getNombreCliente());
		assertEquals(response.getBuyer().getEmailAddress(),
				cliente.getCorreoCliente());
		assertTrue(!response.getTransactions().isEmpty());
		for (Transaction transaction : response.getTransactions()) {
			assertNotNull(transaction);
			assertNotNull(transaction.getPayer());
			assertEquals(transaction.getPayer().getDniNumber(),
					cliente.getIdCliente());
			assertEquals(transaction.getPayer().getFullName(),
					cliente.getNombreCliente());
			assertEquals(transaction.getPayer().getEmailAddress(),
					cliente.getCorreoCliente());
		}
	}

	@Test
	public void testEjecutarReembolso() {
		transaccion.setIdOrderPayU("7670933");
		transaccion
				.setIdTransactionPayU("0a2ac541-ca49-484c-8b2b-9a8fdbe1adbe");
		TransactionResponse response = PayuSDKUtil.ejecutarReembolso(
				transaccion, "Prueba");
		assertNotNull(response);
		assertEquals(response.getOrderId(),
				Integer.valueOf(transaccion.getIdOrderPayU()));
		assertEquals(response.getState(), TransactionState.APPROVED);
		// Consultar nuevo estado de la orden
		RespuestaConsultaOrdenPayU orderResponse = PayuAPIUtil
				.consultarEstadoOrderPayUPorId(transaccion.getIdOrderPayU());
		assertNotNull(orderResponse);
		assertNull(orderResponse.getError());
		Map<String, Object> datosOrden = (HashMap<String, Object>) orderResponse
				.getResult().getPayload();
		assertTrue(!datosOrden.isEmpty());
		assertEquals((Integer) datosOrden.get("id"),
				Integer.valueOf(transaccion.getIdOrderPayU()));
		OrderStatus estadoOrden = OrderStatus.valueOf((String) datosOrden
				.get("status"));
		assertTrue(estadoOrden == OrderStatus.REFUNDED);
	}

	@Test
	public void testEjecutarReembolsoPorAPI() {
		transaccion.setIdOrderPayU("7673587");
		transaccion
				.setIdTransactionPayU("4b5881b1-12b0-44f7-9d5f-d36082cf79e1");
		RespuestaReembolsoPayU respuestaReembolso = PayuAPIUtil
				.reembolsarOrderPayU(transaccion, "Prueba");
		assertNotNull(respuestaReembolso);
		assertNull(respuestaReembolso.getError());
		assertNotNull(respuestaReembolso.getTransactionResponse());
		assertEquals(respuestaReembolso.getTransactionResponse().getOrderId(),
				Integer.valueOf(transaccion.getIdOrderPayU()));
		assertEquals(respuestaReembolso.getTransactionResponse().getState(),
				TransactionState.APPROVED);
		// Consultar nuevo estado de la orden
		RespuestaConsultaOrdenPayU orderResponse = PayuAPIUtil
				.consultarEstadoOrderPayUPorId(transaccion.getIdOrderPayU());
		assertNotNull(orderResponse);
		assertNull(orderResponse.getError());
		Map<String, Object> datosOrden = (HashMap<String, Object>) orderResponse
				.getResult().getPayload();
		assertTrue(!datosOrden.isEmpty());
		assertEquals((Integer) datosOrden.get("id"),
				Integer.valueOf(transaccion.getIdOrderPayU()));
		OrderStatus estadoOrden = OrderStatus.valueOf((String) datosOrden
				.get("status"));
		assertTrue(estadoOrden == OrderStatus.REFUNDED);
	}

	@Test
	public void testConsultarEstadoOrderPayUPorId() throws JsonParseException,
			JsonMappingException, IOException {
		transaccion.setIdOrderPayU("7670892");
		transaccion
				.setIdTransactionPayU("a0784697-79e4-4549-95d8-fda3a3d2e8e6");// 1162541b-0a67-43ba-b15a-1a98732d0c88
		RespuestaConsultaOrdenPayU response = PayuAPIUtil
				.consultarEstadoOrderPayUPorId(transaccion.getIdOrderPayU());
		assertNotNull(response);
		assertNull(response.getError());
		// Leer informacion orden
		Map<String, Object> datosOrden = (HashMap<String, Object>) response
				.getResult().getPayload();
		assertTrue(!datosOrden.isEmpty());
		assertEquals((Integer) datosOrden.get("id"),
				Integer.valueOf(transaccion.getIdOrderPayU()));
		OrderStatus estadoOrden = OrderStatus.valueOf((String) datosOrden
				.get("status"));
		assertTrue(estadoOrden != OrderStatus.CANCELLED
				&& estadoOrden != OrderStatus.DECLINED
				&& estadoOrden != OrderStatus.REFUNDED);
		// Leer informacion Comprador
		Map<String, Object> datosComprador = (HashMap<String, Object>) datosOrden
				.get("buyer");
		assertTrue(!datosComprador.isEmpty());
		assertEquals((String) datosComprador.get("merchantBuyerId"),
				cliente.getIdCliente());
		assertEquals((String) datosComprador.get("fullName"),
				cliente.getNombreCliente());
		assertEquals((String) datosComprador.get("emailAddress"),
				cliente.getCorreoCliente());
		// Leer informacion de Transacciones
		List<HashMap<String, Object>> transacciones = (ArrayList<HashMap<String, Object>>) datosOrden
				.get("transactions");
		assertTrue(!transacciones.isEmpty());
		for (HashMap<String, Object> transaction : transacciones) {
			assertTrue(!transaction.isEmpty());
			assertEquals((String) transaction.get("id"),
					transaccion.getIdTransactionPayU());
			// Lee informacion del pagador
			Map<String, Object> datosPagador = (HashMap<String, Object>) transaction
					.get("payer");
			assertTrue(!datosPagador.isEmpty());
			assertEquals((String) datosPagador.get("dniNumber"),
					cliente.getIdCliente());
			assertEquals((String) datosPagador.get("fullName"),
					cliente.getNombreCliente());
			assertEquals((String) datosPagador.get("emailAddress"),
					cliente.getCorreoCliente());
			// Lee informacion del estado de la transaccion
			Map<String, Object> datosEstadoTransaccion = (HashMap<String, Object>) transaction
					.get("transactionResponse");
			assertTrue(!datosEstadoTransaccion.isEmpty());
			TransactionState estadoTransaccion = TransactionState
					.valueOf((String) datosEstadoTransaccion.get("state"));
			assertTrue(TransactionState.DECLINED != estadoTransaccion
					&& estadoTransaccion != TransactionState.ERROR);
		}
	}

	@Test
	public void testEjecutarPing() {
		assertTrue(PayuSDKUtil.ejecutarPing());
	}

}
