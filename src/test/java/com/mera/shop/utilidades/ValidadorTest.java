/**
 * 
 */
package com.mera.shop.utilidades;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * 
 * @author <a href="david.mera@payulatam.com">David Mera</a>
 * @since 2015-12-8
 * @version 1
 */
public class ValidadorTest {

	private final String NUMEROVALIDO1 = "4916774337416361";
	private final String NUMEROVALIDO2 = "4559861894674904";
	private final String NUMEROINVALIDO1 = "4916774337416362";
	private final String NUMEROINVALIDO2 = "45598618946749036";

	/**
	 * Test method for
	 * {@link com.mera.shop.utilidades.ValidadorUtil#isNumeroTarjetaCreditoValido(java.lang.String)}
	 * .
	 */
	@Test
	public void testIsNumeroTarjetaCreditoValido() {
		assertTrue(ValidadorUtil.isNumeroTarjetaCreditoValido(NUMEROVALIDO1));
		assertTrue(ValidadorUtil.isNumeroTarjetaCreditoValido(NUMEROVALIDO2));
		assertTrue(!ValidadorUtil
				.isNumeroTarjetaCreditoValido(NUMEROINVALIDO1));
		assertTrue(!ValidadorUtil
				.isNumeroTarjetaCreditoValido(NUMEROINVALIDO2));
	}

	/**
	 * Test method for
	 * {@link com.mera.shop.utilidades.ValidadorUtil#isTarjetaCreditoValida(com.mera.shop.utilidades.FranquiciaTarjetaCredito, java.lang.String)}
	 * .
	 */
	@Test
	public void testIsTarjetaCreditoValida() {
		assertTrue(ValidadorUtil.isTarjetaCreditoValida(FranquiciaTarjetaCredito.VISA, NUMEROVALIDO1));
		assertTrue(ValidadorUtil.isTarjetaCreditoValida(FranquiciaTarjetaCredito.VISA, NUMEROVALIDO2));
		assertTrue(!ValidadorUtil.isTarjetaCreditoValida(FranquiciaTarjetaCredito.VISA, NUMEROINVALIDO1));
		assertTrue(!ValidadorUtil.isTarjetaCreditoValida(FranquiciaTarjetaCredito.VISA, NUMEROINVALIDO2));
	}

}
